var AlertModel = new joli.model({
    table : 'alerts',
    columns : {
        id : 'INTEGER PRIMARY KEY',
        name : 'TEXT',
        zone: 'INTEGER',
        alert_type: 'INTEGER',
        created: 'TEXT'
    },
    methods : {
        fetch : function(success, error) {
            var routeTimeout = setTimeout(function() {
                  if (!finished) {
                      Ti.API.error("timeout exception");
                      if (error) {
                          error();
                      }
                  }
              }, 45000);
            var that = this;
            // create a progress indicator
            var finished = false;
            // var last = Ti.App.Properties.getInt('lastAlert', 0);
            var client = Ti.Network.createHTTPClient({
                onload : function(e) {
                    that.truncate();
                    Ti.API.error("routes fetched");
                    clearTimeout(routeTimeout);
                    var data = JSON.parse(this.responseText);
                    // finished = true;
                    _(data.objects).each(function(alertData) {
                        var route = that.createRecord(alertData);
                    });
                    if (data.objects.length > 0) {
                        Ti.App.Properties.setInt('lastAlert', data.objects[data.objects.length - 1].id);
                    }
                    Ti.App.fireEvent('alerts.fetched');
                    if(success) {
                        success();
                    }
                },
                onerror : function(e) {
                    clearTimeout(routeTimeout);
                    // finished = true;
                    if(error) {
                        error();
                    }
                }
                // ondatastream : function(e) {
                //     Ti.API.info('On Data Stream: ' + e.progress);
                // }
            });
            Ti.API.error(JSON.stringify(APP));
            Ti.API.error(JSON.stringify(APP.URL));
            client.setTimeout(30000);
            client.open("GET", APP.URL + "alert_list.php?last=0");
            // var password = Ti.App.Properties.getString('password');
            // var username = Ti.App.Properties.getString('username');
            // client.setRequestHeader("Authorization", "Basic " + Ti.Utils.base64encode(username + ":" + password));
            client.send();
        },
        createRecord : function(data) {
          Ti.API.info("CreateRecord route********************************");
            var query = new joli.query().insertReplace('alerts').values({
                id : data.id,
                name : data.name,
                zone : data.zone,
                alert_type: data.alert_type,
                created: data.created
            }).execute();
            return data;
        },
        isEmpty : function() {
            return this.count() === 0;
        },
        getAll: function(id) {
            var query = new joli.query()
                .select()
                .from('alerts')
                .order('created desc')
                .limit('50')
                .execute();
            return query;
        },
    },
    objectMethods: {}
});

module.exports = AlertModel;
