/**
 * The main app singleton used throughout the app. This object contains static
 * properties, global event handling, etc.
 *
 * @class core
 * @singleton
 * @uses utilities
 * @uses http
 * @uses migrate
 * @uses update
 * @uses push
 * @uses Modules.ti.cloud
 */
var UTIL = require("utilities");
var AlertModel = require("models/alert");
// var CloudPush = require('ti.cloudpush');
// var Cloud = require("ti.cloud");
// var PushClient = require('br.com.arlsoft.pushclient');
// var registerOptions = {
//     GCMSenderId : '485936580139',
//     APNTypes : [ PushClient.NOTIFICATION_TYPE_BADGE, PushClient.NOTIFICATION_TYPE_ALERT, PushClient.NOTIFICATION_TYPE_SOUND ]
// };
var gcm = require('net.iamyellow.gcmjs');

var APP = {
    /**
     * Application ID
     * @type {String}
     */
    ID: null,
    /**
     * Base version
     * @type {String}
     */
    VERSION: null,
    /**
     * Kliento aplication version
     * @type {String}
     */
    APPVERSION: null,
    DEVICETOKEN: null,
    COPY: null,
    /**
     * URL to remote JSON configuration file
     *
     * **NOTE: This can be used for over-the-ai r (OTA) application updates.**
     * @type {String}
     */
    URL: "http://botonrojo.com/semaforo/",
    // URL: "http://192.168.0.100/",
    /**
     * All the component nodes (e.g. tabs)
     * @type {Object}
     */
    Nodes: [
        {
            "title": "Configurar alertas",
            "type": "config"
        },
        {
            "title": "Tipos de alertas",
            "type": "help"
        },
        {
            "title": "Términos y condiciones",
            "type": "terms"
        },
        {
            "title": "Cerrar sesión",
            "type": "close"
        },

    ],
    /**
     * Application settings as defined in JSON configuration file
     * @type {Object}
     * @param {String} share The share text
     * @param {Object} notifications Push notifications options
     * @param {Boolean} notifications.enabled Whether or not push notifications are enabled
     * @param {String} notifications.provider Push notifications provider
     * @param {String} notifications.key Push notifications key
     * @param {String} notifications.secret Push notifications secret
     * @param {Object} colors Color options
     * @param {String} colors.primary The primary color
     * @param {String} colors.secondary The secondary color
     * @param {String} colors.theme The theme of the primary color, either "light" or "dark"
     * @param {Object} colors.hsb The HSB values of the primary color
     * @param {Boolean} useSlideMenu Whether or not to use the slide menu (alternative is tabs)
     */
    Settings: null,
    /**
     * Device information
     * @type {Object}
     * @param {Boolean} isHandheld Whether the device is a handheld
     * @param {Boolean} isTablet Whether the device is a tablet
     * @param {String} type The type of device, either "handheld" or "tablet"
     * @param {String} os The name of the OS, either "IOS" or "ANDROID"
     * @param {String} name The name of the device, either "IPHONE", "IPAD" or the device model if Android
     * @param {String} version The version of the OS
     * @param {Number} versionMajor The major version of the OS
     * @param {Number} versionMinor The minor version of the OS
     * @param {Number} width The width of the device screen
     * @param {Number} height The height of the device screen
     * @param {Number} dpi The DPI of the device screen
     * @param {String} orientation The device orientation, either "LANDSCAPE" or "PORTRAIT"
     * @param {String} statusBarOrientation A Ti.UI orientation value
     */
    Device: {
        isHandheld: Alloy.isHandheld,
        isTablet: Alloy.isTablet,
        type: Alloy.isHandheld ? "handheld" : "tablet",
        os: null,
        name: null,
        version: Ti.Platform.version,
        versionMajor: parseInt(Ti.Platform.version.split(".")[0], 10),
        versionMinor: parseInt(Ti.Platform.version.split(".")[1], 10),
        width: Ti.Platform.displayCaps.platformWidth > Ti.Platform.displayCaps.platformHeight ? Ti.Platform.displayCaps.platformHeight : Ti.Platform.displayCaps.platformWidth,
        height: Ti.Platform.displayCaps.platformWidth > Ti.Platform.displayCaps.platformHeight ? Ti.Platform.displayCaps.platformWidth : Ti.Platform.displayCaps.platformHeight,
        dpi: Ti.Platform.displayCaps.dpi,
        orientation: Ti.Gesture.orientation == Ti.UI.LANDSCAPE_LEFT || Ti.Gesture.orientation == Ti.UI.LANDSCAPE_RIGHT ? "LANDSCAPE" : "PORTRAIT",
        statusBarOrientation: null
    },
    /**
     * Network status and information
     * @type {Object}
     * @param {String} type Network type name
     * @param {Boolean} online Whether the device is connected to a network
     */
    Network: {
        type: Ti.Network.networkTypeName,
        online: Ti.Network.online
    },
    /**
     * Current controller view stack index
     * @type {Number}
     */
    currentStack: -1,
    /**
     * The previous screen in the hierarchy
     * @type {Object}
     */
    previousScreen: null,
    /**
     * The view stack for controllers
     * @type {Array}
     */
    controllerStacks: [],
    /**
     * The view stack for modals
     * @type {Array}
     */
    modalStack: [],
    /**
     * Whether or not the current view has a tablet layout
     * @type {Boolean}
     */
    hasDetail: false,
    /**
     * Current detail view stack index
     * @type {Number}
     */
    currentDetailStack: -1,
    /**
     * The previous detail screen in the hierarchy
     * @type {Object}
     */
    previousDetailScreen: null,
    /**
     * The view stack for detail views
     * @type {Array}
     */
    detailStacks: [],
    /**
     * The view stack for master views
     * @type {Array}
     */
    Master: [],
    /**
     * The view stack for detail views
     * @type {Array}
     */
    Detail: [],
    /**
     * The main app window
     * @type {Object}
     */
    MainWindow: null,
    /**
     * The global view all screen controllers get added to
     * @type {Object}
     */
    GlobalWrapper: null,
    /**
     * The global view all content screen controllers get added to
     * @type {Object}
     */
    ContentWrapper: null,
    /**
     * The map view a
     * @type {Object}
     */
    mapView: null,
    /**
     * Holder for ACS cloud module
     * @type {Object}
     */
    // ACS: null,
    /**
     * The loading view
     * @type {Object}
     */
    Loading: Alloy.createWidget("com.kliento.loading"),
    /**
     * Whether or not to cancel the loading screen open because it's already open
     * @type {Boolean}
     */
    cancelLoading: false,
    /**
     * Whether or not the loading screen is open
     * @type {Boolean}
     */
    loadingOpen: false,
    /**
     * Tabs widget
     * @type {Object}
     */
    Tabs: null,
    /**
     * Slide Menu widget
     * @type {Object}
     */
    SlideMenu: null,
    /**
     * Whether or not the slide menu is open
     * @type {Boolean}
     */
    SlideMenuOpen: false,
    /**
     * Whether or not the slide menu is engaged
     *
     * **NOTE: Turning this false temporarily disables the slide menu**
     * @type {Boolean}
     */
    SlideMenuEngaged: true,
    /* Whether or not the slide menu is engaged
     *
     * @type {Boolean}
     */
    LoggedIn: Ti.App.Properties.getBool('loggedIn', false),
    /**
     * Initializes the application
     */
    init: function() {
        Ti.API.error("APP.init");
        if(OS_ANDROID) {
            APP.MainWindow.addEventListener("androidback", APP.backButtonObserver);
        }

        APP.determineDevice();
        APP.setupDatabase();
        APP.build();
    },
    /**
     * Determines the device characteristics
     */
    determineDevice: function() {
        if(OS_IOS) {
            APP.Device.os = "IOS";

            if(Ti.Platform.osname.toUpperCase() == "IPHONE") {
                APP.Device.name = "IPHONE";
            } else if(Ti.Platform.osname.toUpperCase() == "IPAD") {
                APP.Device.name = "IPAD";
            }
        } else if(OS_ANDROID) {
            APP.Device.os = "ANDROID";

            APP.Device.name = Ti.Platform.model.toUpperCase();

            // Fix the display values
            APP.Device.width = (APP.Device.width / (APP.Device.dpi / 160));
            APP.Device.height = (APP.Device.height / (APP.Device.dpi / 160));
        }
    },
    /**
     * Setup the database bindings
     */
    setupDatabase: function(callback) {
        Ti.API.error("APP.setupDatabase");
        joli.models.initialize();
    },
    /**
     * Drops the entire database
     */
    dropDatabase: function() {
        APP.log("error", "APP.dropDatabase");
        var models = ['answers', 'catalogs', 'catalogsitems', 'choices', 'conditionals', 'errorPing', 'questions', 'responses', 'routes', 'route_events', 'route_points', 'route_responses', 'sections', 'surveys', 'update'];
        _(models).each(function(modelName) {
            var query = 'DROP TABLE IF EXISTS `' + modelName + '`';
            joli.connection.execute(query);
        });
        // joli.connection.disconnect();
        // joli = require('vendor/joli').connect('Kliento');
        // var db = Ti.Database.open("Kliento");
        // db.remove();
    },
    /**
     * Builds out the tab group
     */
    build: function() {
        APP.log("error", "APP.build");
        APP.MainWindow.open();

        if (!APP.LoggedIn) {
            APP.log("error", "Login window");
            APP.addChild('login', {}, false);
        } else {
            Ti.Android.NotificationManager.cancelAll();
            var pendingData = gcm.data;
            if (pendingData && pendingData !== null) {
                APP.addChild('list', {}, false);
                APP.addChild('alert', pendingData, true);
            } else {
                var SYNC = require("sync");
                SYNC.fetch(function(){
                    APP.addChild('list', {}, false);
                });
            }
            APP.checkNotifications();
        }
    },
    checkNotifications: function() {
        Ti.API.error("notification");
        gcm.registerForPushNotifications({
            success: function (ev) {
                // on successful registration
                Ti.API.info('******* success, ' + ev.deviceToken);
                var client = Ti.Network.createHTTPClient({
                    onload : function(e) {
                        Ti.API.error("Dispositivo registrado", ev.deviceToken, Ti.Platform.id);
                    },
                    onerror : function(e) {
                        alert("No se puede conectar al servicio");
                    }
                });
                client.setTimeout(30000);
                client.open("POST", APP.URL + "app_device_token.php");
                client.send(JSON.stringify({id: Ti.App.Properties.getInt('userId'), token:ev.deviceToken, deviceId:Ti.Platform.id}));
            },
            error: function (ev) {
                alert("Notificaciones no disponibles");
                Ti.API.error('******* error, ' + ev.error);
            },
            callback: function (data) {
                // when a gcm notification is received WHEN the app IS IN FOREGROUND
                Ti.API.error(JSON.stringify(data));
                AlertModel.createRecord(data);
                Ti.App.Properties.setInt('lastAlert', data.id);
                Ti.App.fireEvent('alerts.fetched');
            },
            unregister: function (ev) {
                // on unregister
                Ti.API.error('******* unregister, ' + ev.deviceToken);
            },
            data: function (data) {
                // if we're here is because user has clicked on the notification
                // and we set extras in the intent
                // and the app WAS RUNNING (=> RESUMED)
                // (again don't worry, we'll see more of this later)
                Ti.Android.NotificationManager.cancelAll();
                APP.addChild('alert', data, true);
                Ti.API.error('******* data (resumed) ' + JSON.stringify(data));
            }
        });
    },
    /**
     * Builds a slide menu
     * @param {Array} _nodes The items (menu nodes) to build
     */
    buildMenu: function(_nodes) {
        APP.log("debug", "APP.buildMenu");

        APP.SlideMenu.init({
            nodes: _nodes,
            color: {
                headingBackground: "#e6e6e6",
                headingText: "#000"
            }
        });

        // Add a handler for the nodes (make sure we remove existing ones first)
        APP.SlideMenu.Nodes.removeEventListener("click", APP.handleMenuClick);
        APP.SlideMenu.Nodes.addEventListener("click", APP.handleMenuClick);
    },
    /**
     * Handles the click event on a tab
     * @param {Object} _event The event
     */
    handleTabClick: function(_event) {
        if(typeof _event.source.id !== "undefined" && typeof _event.source.id == "number") {
            APP.handleNavigation(_event.source.id);
        }
    },
    /**
     * Handles the click event on a menu item
     * @param {Object} _event The event
     */
    handleMenuClick: function(_event) {
        if(typeof _event.row.id !== "undefined" && typeof _event.row.id == "number") {
            APP.closeSettings();

            APP.handleNavigation(_event.row.id);
        } else if(typeof _event.row.id !== "undefined" && _event.row.id == "settings") {
            APP.openSettings();
        } else if(typeof _event.row.id !== "undefined" && _event.row.id == "exit") {
            dialogs.confirm({
                    title: "Cerrar Aplicación",
                    message: "¿Está seguro que desea salir de la aplicación? Con esto perderá comunicación con la empresa",
                    callback: function() {
                        Alloy.createWidget("com.mcongrove.toast", null, {
                                text: "Cerrando aplicación",
                                duration: 2000,
                                view: APP.GlobalWrapper
                        });
                    Ti.App.fireEvent('app.exit');
                    APP.exitApp();
                    }
            });
        }
        APP.toggleMenu();
    },
    /**
     * Open a child screen
     * @param {String} _controller The name of the controller to open
     * @param {Object} _params An optional dictionary of parameters to pass to the controller
     * @param {Boolean} _sibling Whether this is a sibling view
     */
    addChild: function(_controller, _params, _sibling) {
        // Create the new screen controller
        var screen = Alloy.createController(_controller, _params).getView();

        if(!_sibling) {
            APP.removeAllChildren();
            APP.controllerStacks = [];
        }

        // Add screen to the controller stack
        APP.controllerStacks.push(screen);
        // Add the screen to the window
        APP.addScreen(screen);
    },
    /**
     * Removes a child screen
     * @param {Boolean} _modal Removes the child from the modal stack
     */
    removeChild: function() {
        APP.removeScreen(APP.controllerStacks.pop());
    },
    /**
     * Removes all children screens
     * @param {Boolean} _modal Removes all children from the stack
     */
    removeAllChildren: function(_modal) {
        APP.controllerStacks = [];
        APP.GlobalWrapper.removeAllChildren();
    },
    /**
     * Global function to add a screen
     * @param {Object} _screen The screen to add
     */
    addScreen: function(_screen) {
        if(_screen) {
            APP.GlobalWrapper.add(_screen);
        }
    },
    /**
     * Global function to remove a screen
     * @param {Object} _screen The screen to remove
     */
    removeScreen: function(_screen) {
        if(_screen) {
            APP.GlobalWrapper.remove(_screen);
        }
    },
    /**
     * Opens the Settings window
     */
    openSettings: function() {
        APP.log("error", "APP.openSettings");

        APP.addChild("settings", {}, true);
    },
    /**
     * Closes all non-tab stacks
     */
    closeSettings: function() {
        if(APP.modalStack.length > 0) {
            APP.removeChild(true);
        }
    },
    /**
     * Toggles the Slide Menu
     */
    toggleMenu: function(_position) {
        Ti.API.error("toggle menu");
        if(APP.SlideMenuOpen) {
            APP.closeMenu();
        } else {
            APP.openMenu();
        }
    },
    /**
     * Opens the Slide Menu
     */
    openMenu: function() {
        Ti.API.error("open menu");
        // APP.SlideMenu.Wrapper.left = "0dp";

        APP.addScreen(APP.SlideMenu.Wrapper);

        // APP.GlobalWrapper.animate({
        //     left: "200dp",
        //     duration: 250,
        //     curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
        // });

        APP.SlideMenuOpen = true;
    },
    /**
     * Closes the Slide Menu
     */
    closeMenu: function() {
        Ti.API.error("close menu");
        APP.removeScreen(APP.SlideMenu.Wrapper);
        // APP.GlobalWrapper.animate({
        //     left: "0dp",
        //     duration: 250,
        //     curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
        // });

        APP.SlideMenuOpen = false;
    },
    /**
     * Shows the loading screen
     */
    openLoading: function(message) {
        APP.cancelLoading = false;

        if(!APP.cancelLoading) {
            APP.loadingOpen = true;
            APP.Loading.init(message);
            APP.MainWindow.add(APP.Loading.getView());
        }
    },
    /**
     * Closes the loading screen
     */
    closeLoading: function() {
        APP.cancelLoading = true;

        if(APP.loadingOpen) {
            APP.MainWindow.remove(APP.Loading.getView());
            APP.loadingOpen = false;
        }
    },
    /**
     * Logs all console data
     * @param {String} _severity A severity type (debug, error, info, log, trace, warn)
     * @param {String} _text The text to log
     */
    log: function(_severity, _text) {
        switch(_severity.toLowerCase()) {
            case "debug":
                Ti.API.debug(_text);
                break;
            case "error":
                Ti.API.error(_text);
                break;
            case "info":
                Ti.API.info(_text);
                break;
            case "log":
                Ti.API.log(_text);
                break;
            case "trace":
                Ti.API.trace(_text);
                break;
            case "warn":
                Ti.API.warn(_text);
                break;
        }
    },

    /**
     * Global orientation event handler
     * @param {Object} _event Standard Titanium event callback
     */
    orientationObserver: function(_event) {
        APP.log("debug", "APP.orientationObserver");

        if(APP.Device.statusBarOrientation && APP.Device.statusBarOrientation == _event.orientation) {
            return;
        }

        APP.Device.statusBarOrientation = _event.orientation;

        APP.Device.orientation = (_event.orientation == Ti.UI.LANDSCAPE_LEFT || _event.orientation == Ti.UI.LANDSCAPE_RIGHT) ? "LANDSCAPE" : "PORTRAIT";

        Ti.App.fireEvent("APP:orientationChange");
    },
    /**
     * Back button observer
     * @param {Object} _event Standard Titanium event callback
     */
    backButtonObserver: function(_event) {
        APP.log("error", "APP.backButtonObserver");
        if (APP.loadingOpen || APP.alertIsOpen) {
            return false;
        }

        var stack = APP.controllerStacks;
        if(stack.length > 1) {
            APP.removeChild();
        } else {
            var intent = Ti.Android.createIntent({
                action: Ti.Android.ACTION_MAIN
            });
            intent.addCategory(Ti.Android.CATEGORY_HOME);
            Ti.Android.currentActivity.startActivity(intent);
        }
    }
};

module.exports = APP;