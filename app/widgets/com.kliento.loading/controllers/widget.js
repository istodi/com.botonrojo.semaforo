/**
 * The loading overlay widget
 *
 * @class Widgets.com.kliento.loading
 */
$.init = function(message) {
    $.Loading.start();
    // $.Loading.stop();
    if (typeof(message) !== 'undefined') {
        $.Label.setText(message);
    } else {
        $.Label.setText('Loading');
    }

};
