var SYNC = require("sync");

// var social = require('alloy/social').create({
//     consumerSecret: 'a8K2anLfprlmwuUFxR2iisOyczSPdZTfp9ECXA49AqFvSxSxwj',
//     consumerKey: 'OIlsfXD7zprIVGiTWepA4znNy'
// });

// Twitter setup
var twitterClient = Twitter({
  consumerKey: "OIlsfXD7zprIVGiTWepA4znNy",
  consumerSecret: "a8K2anLfprlmwuUFxR2iisOyczSPdZTfp9ECXA49AqFvSxSxwj"
});


twitterClient.addEventListener('login', function(e) {
  if (e.success) {
    // Here's an example API call:
    twitterClient.request("1.1/account/verify_credentials.json", {}, 'GET', function(e) {
      if (e.success) {
        var jsonData = JSON.parse(e.result.text);
        var data = {
            mail : '',
            pass : '',
            name : jsonData.name,
            login_type: '1',
            twitter_username: jsonData.screen_name
        };
        SYNC.login(data);
      } else {
        alert(e.error);
      }
    });
  } else {
    alert(e.error);
  }
});

// Facebook setup
fb.appid = 619173798190450;
fb.permissions = ["email", "public_profile"];

fb.addEventListener('login', function(e) {
    if (e.success) {
        fb.requestWithGraphPath('me', {}, 'GET', function(e) {
            if (e.success) {
                var responseData = JSON.parse(e.result);
                var data = {
                    mail : responseData.email,
                    pass : '',
                    name : (responseData.first_name + ' ' + responseData.last_name).trim(),
                    login_type: '2',
                    twitter_username: ''
                };
                SYNC.login(data);
            } else if (e.error) {
                alert(e.error);
            } else {
                alert('Unknown response');
            }
        });
    } else if (e.error) {
        alert(e.error);
    }
});

$.init = function() {
    $.loginView.backgroundImage = '/images/bg.png';
};

function loginTW() {
    twitterClient.authorize();
}

function loginFB() {
    fb.authorize();
}

function loginMail() {
    APP.addChild('login_mail', {}, true);
}

Ti.Gesture.addEventListener('orientationchange', function(e) {
    switchOrientation(Titanium.Gesture.orientation);
});

switchOrientation(Titanium.Gesture.orientation);

function switchOrientation(orientation) {
    if (orientation === 2) {
        $.logo.applyProperties({
            top: "22%",
            left: "2%",
            width: "40%",
            height: Ti.UI.SIZE
        });
        $.buttonContainer.applyProperties({
            top: "20%",
            left: "45%",
            right: "2%",
            width: Ti.UI.FILL
        });
    } else if(orientation === 1){
        $.logo.applyProperties({
            top: "7%",
            left: "auto",
            height: "21%",
            width: Ti.UI.SIZE
        });
        $.buttonContainer.applyProperties({
            top: "30%",
            left: "12.5%",
            right: "12.5%",
            width: "75%"
        });
    }
}

$.init();