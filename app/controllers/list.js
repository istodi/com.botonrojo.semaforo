/**
 * Controller for the survey list screen
 *
 * @class Controllers.survey.list
 * @uses core
 */
var MODEL = require("models/alert");

var CONFIG = arguments[0];  // Get arguments from his parent
var rows = [];
/**
 * Initializes the controller
 */
$.init = function() {
    APP.log("error", "alerts.init");
    $.NavigationBar.showMenu(function(_event) {
        _event.cancelBubble = true;
        $.NavigationBar.toggleMenu();
    });

    data = MODEL.getAll();
    // Validate: if is just one object go to render that
    $.handleData(data);
};

Ti.App.addEventListener('alerts.fetched', function() {
    APP.log("error", "actualizar lista de rutas");
    $.handleData(MODEL.getAll());
});

/**
 * Handles the data return
 * @param {Object} _data The returned data
 */
$.handleData = function(data) {
    APP.log("error", "routes.handleData");

    rows = [];

    _(data).each(function(_datum) {
        var row = Alloy.createController("row", {
            id: _datum.id,
            name: _datum.name,
            alert_type: _datum.alert_type,
            created: _datum.created
        }).getView();

        _datum.isChild = true;

        row.rowData = _datum;

        rows.push(row);
    });

    $.container.setData(rows);
};

// Kick off the init
$.init();
