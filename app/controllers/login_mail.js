var SYNC = require("sync");
var CONFIG = arguments[0];

$.init = function() {
    Ti.API.error("init");
    $.loginView.backgroundImage = '/images/bg.png';
    $.mail.KeyboardType = Ti.UI.KEYBOARD_EMAIL;
};

function login() {
    var valid = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test($.mail.value);
    if (valid) {
        if ($.password.value === $.passwordConfirmation.value) {
            var data = {
                mail : $.mail.value,
                pass : Ti.Utils.sha256($.password.value),
                name : '',
                login_type: '3',
                twitter_username: ''
            };
            SYNC.login(data);
        } else {
            alert("Las contraseñas son diferentes");
        }
    } else {
        alert("Correo electrónico invalido");
    }
}

function showTerms() {
    APP.addChild('terms', {'menu': false}, true);
}

Ti.Gesture.addEventListener('orientationchange', function(e) {
    switchOrientation(Titanium.Gesture.orientation);
});

switchOrientation(Titanium.Gesture.orientation);

function switchOrientation(orientation) {
    if (orientation === 2) {
        $.logo.applyProperties({
            top: "22%",
            left: "2%",
            width: "40%",
            height: Ti.UI.SIZE
        });
        $.buttonContainer.applyProperties({
            top: "5%",
            left: "45%",
            right: "2%",
            width: Ti.UI.FILL
        });
    } else if(orientation === 1){
        $.logo.applyProperties({
            top: "7%",
            left: "auto",
            height: "21%",
            width: Ti.UI.SIZE
        });
        $.buttonContainer.applyProperties({
            top: "30%",
            left: "12.5%",
            right: "12.5%",
            width: "75%"
        });
    }
}

// $.loginView.addEventListener('click', function(event) {
//     Ti.API.error(JSON.stringify(event.source));
//     Ti.API.error(typeof(event.source));
//     Ti.API.error(typeof(event.source.hintText));
//     if (typeof(event.source.hintText) !== 'string') {
//         Ti.API.error("cierrame");
//         $.mail.blur();
//         $.password.blur();
//         $.passwordConfirmation.blur();
//     }
// });

$.init();