$.init = function() {
    $.NavigationBar.showMenu(function(_event) {
        _event.cancelBubble = true;
        $.NavigationBar.toggleMenu();
    });

    $.NavigationBar.showBack(function(_event) {
        _event.cancelBubble = true;
        APP.removeChild();
    });
};

$.init();