var SYNC = require("sync");
var GREEN = "#049244";
var YELLOW = "#faa530";
var ORANGE = "#EF5A22";
var RED = "#C0272A";
var SELECTED = "#4d4d4d";

var CONFIG = arguments[0];

$.init = function() {
    Ti.API.error("show config");
    var alert1 = Ti.App.Properties.getBool('alert_1', true);
    if (!alert1) {
        $.green.setBackgroundColor('#F1F1F1');
        $.alert1.setColor("#a3a3a3");
        $.check1.text = '';
    }

    var alert2 = Ti.App.Properties.getBool('alert_2', true);
    if (!alert2) {
        $.yellow.setBackgroundColor('#F1F1F1');
        $.alert2.setColor("#a3a3a3");
        $.check2.text = '';
    }

    var alert3 = Ti.App.Properties.getBool('alert_3', true);
    if (!alert3) {
        $.orange.setBackgroundColor('#F1F1F1');
        $.alert3.setColor("#a3a3a3");
        $.check3.text = '';
    }

    var alert4 = Ti.App.Properties.getBool('alert_4', true);
    if (!alert4) {
        $.red.setBackgroundColor('#F1F1F1');
        $.alert4.setColor("#a3a3a3");
        $.check4.text = '';
    }

    if (CONFIG.hasOwnProperty('menu') ? CONFIG.back : true) {
        $.NavigationBar.showMenu(function(_event) {
            _event.cancelBubble = true;
            $.NavigationBar.toggleMenu();
        });
    }

    if (CONFIG.hasOwnProperty('back') ? CONFIG.back : true) {
        $.NavigationBar.showBack(function(_event) {
            _event.cancelBubble = true;
            APP.removeChild();
        });
    }
};

$.green.addEventListener('click', function(e) {
    setChecked({
        'item': $.green,
        'backGroundColor': GREEN,
        'buttonTxt': $.alert1,
        'buttonCheck': $.check1,
        'notification': 'alert_1'
    });
});

$.yellow.addEventListener('click', function(e) {
    setChecked({
        'item': $.yellow,
        'backGroundColor': YELLOW,
        'buttonTxt': $.alert2,
        'buttonCheck': $.check2,
        'notification': 'alert_2'
    });
});

$.orange.addEventListener('click', function(e) {
    setChecked({
        'item': $.orange,
        'backGroundColor': ORANGE,
        'buttonTxt': $.alert3,
        'buttonCheck': $.check3,
        'notification': 'alert_3'
    });
});

$.red.addEventListener('click', function(e) {
    setChecked({
        'item': $.red,
        'backGroundColor': RED,
        'buttonTxt': $.alert4,
        'buttonCheck': $.check4,
        'notification': 'alert_4'
    });
});

// $.allZones.addEventListener('click', function(e) {
//     setChecked({
//         'item': $.allZones,
//         'backGroundColor': SELECTED,
//         'buttonTxt': $.allZonesTxt,
//         'buttonCheck': $.allZonesCheck
//     });
// });

// $.closeZones.addEventListener('click', function(e) {
//     setChecked({
//         'item': $.closeZones,
//         'backGroundColor': SELECTED,
//         'buttonTxt': $.closeZonesTxt,
//         'buttonCheck': $.closeZonesCheck
//     });
// });

function setChecked(args) {
    if (args.item.getBackgroundColor() === '#F1F1F1') {
        Ti.App.Properties.setBool(args.notification, true);
        args.item.setBackgroundColor(args.backGroundColor);
        args.buttonCheck.setColor("#FFF");
        args.buttonTxt.setColor("#FFF");
        args.buttonCheck.text = '\u2714';
    } else {
        Ti.App.Properties.setBool(args.notification, false);
        args.item.setBackgroundColor('#F1F1F1');
        args.buttonCheck.setColor("#a3a3a3");
        args.buttonTxt.setColor("#a3a3a3");
        args.buttonCheck.text = '';
    }
}

function save() {
    if (CONFIG.hasOwnProperty('back') && !CONFIG.back) {
        SYNC.fetch(function() {
            APP.addChild('list', {}, false);
        });
    } else {
        APP.removeChild();
    }
}

$.init();