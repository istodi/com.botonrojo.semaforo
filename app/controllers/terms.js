var CONFIG = arguments[0];

$.init = function() {
    if (CONFIG.hasOwnProperty('menu') ? CONFIG.back : true) {
        $.NavigationBar.showMenu(function(_event) {
            _event.cancelBubble = true;
            $.NavigationBar.toggleMenu();
        });
    }

    if (CONFIG.hasOwnProperty('back') ? CONFIG.back : true) {
        $.NavigationBar.showBack(function(_event) {
            _event.cancelBubble = true;
            APP.removeChild();
        });
    }
};

$.init();