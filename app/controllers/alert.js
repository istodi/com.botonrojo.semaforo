/**
 * Controller for the survey list screen
 *
 * @class Controllers.survey.list
 * @uses core
 */
var SYNC = require("sync");
var CONFIG = arguments[0];  // Get arguments from his parent
var BACKGROUNDS = ["", "#049244", "#faa530", "#EF5A22", "#C0272A"];
/**
 * Initializes the controller
 */
$.init = function() {
    APP.log("error", "alert.detail.init");
    var alert_type = parseInt(CONFIG.alert_type, 10);
    $.container.setBackgroundColor(BACKGROUNDS[alert_type]);
    $.alertLogo.setImage('/images/alert_' + alert_type + '.png');
    $.message.setText(CONFIG.message);
    $.created.setText(moment(CONFIG.created).format('dddd DD/MM/YYYY - h:mm A'));
};


function showList() {
    SYNC.fetch(function() {
        APP.removeChild();
    });
}

// Kick off the init
$.init();
