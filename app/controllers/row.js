/**
 * Controller for the routes table row
 *
 * @class Controllers.routes.row
 * @uses core
 */
var CONFIG = arguments[0] || {};

var row_1 = $.createStyle({
    classes: 'row_1'
});
var row_2 = $.createStyle({
    classes: 'row_2'
});
var row_3 = $.createStyle({
    classes: 'row_3'
});
var row_4 = $.createStyle({
    classes: 'row_4'
});
var alert_type = CONFIG.alert_type;
switch(alert_type) {
    case 1:
        $.border.applyProperties(row_1);
        $.alertIcon.image = '/images/alert1.png';
    break;
    case 2:
        $.border.applyProperties(row_2);
        $.alertIcon.image = '/images/alert2.png';
    break;
    case 3:
        $.border.applyProperties(row_3);
        $.alertIcon.image = '/images/alert3.png';
    break;
    case 4:
        $.border.applyProperties(row_4);
        $.alertIcon.image = '/images/alert4.png';
    break;
}

$.name.text = CONFIG.name || '';
$.created.text = moment(CONFIG.created).format('dddd DD/MM/YYYY - h:mm A');
