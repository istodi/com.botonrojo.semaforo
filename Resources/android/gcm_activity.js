var _ = require("alloy/underscore")._;

var Alloy = require("alloy");

var joli = require("vendor/joli").connect("semaforo");

var fb = require("facebook");

var Twitter = require("twitter").Twitter;

var moment = require("/vendor/moment");

moment.lang("es");

var APP = require("core");

(function(activity, gcm) {
    var intent = activity.intent;
    intent.hasExtra("ntfId") && (gcm.data = {
        ntfId: intent.getIntExtra("ntfId", 0),
        id: intent.getStringExtra("id", "0"),
        alert_type: intent.getStringExtra("alert_type", ""),
        message: intent.getStringExtra("message", ""),
        created: intent.getStringExtra("created", "")
    });
    if (gcm.isLauncherActivity) {
        var mainActivityIntent = Ti.Android.createIntent({
            className: gcm.mainActivityClassName,
            packageName: Ti.App.id,
            flags: Ti.Android.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Ti.Android.FLAG_ACTIVITY_SINGLE_TOP
        });
        mainActivityIntent.addCategory(Ti.Android.CATEGORY_LAUNCHER);
        activity.startActivity(mainActivityIntent);
    } else activity.finish();
})(Ti.Android.currentActivity, require("net.iamyellow.gcmjs"));