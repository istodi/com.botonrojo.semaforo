function fetch(callback) {
    APP.openLoading("Descargando alertas");
    AlertModel.fetch(function() {
        APP.closeLoading();
        Ti.API.error("synced success");
        callback();
    }, function() {
        APP.closeLoading();
        Ti.API.error("synced failed");
        callback();
    });
}

function login(data) {
    APP.openLoading("Iniciando sesión");
    Ti.API.error(JSON.stringify(data));
    var client = Ti.Network.createHTTPClient({
        onload: function() {
            var responseData = JSON.parse(this.responseText);
            if (responseData.hasOwnProperty("status")) switch (responseData.status) {
              case 0:
                makeLogin(responseData);
                break;

              case 1:
                makeLogin(responseData);
                break;

              case 2:
                alert("Datos incorrectos");
            } else {
                APP.closeLoading();
                alert("No se puede conectar al servicio");
            }
        },
        onerror: function() {
            APP.closeLoading();
            alert("No se puede conectar al servicio");
        }
    });
    client.setTimeout(3e4);
    client.open("POST", APP.URL + "app_auth.php");
    client.send(JSON.stringify(data));
}

var AlertModel = require("models/alert");

var makeLogin = function(data) {
    Ti.App.Properties.setInt("userId", data.id);
    Ti.App.Properties.setBool("loggedIn", true);
    APP.checkNotifications();
    APP.addChild("config", {
        back: false,
        menu: false
    }, false);
    APP.closeLoading();
};

exports.fetch = fetch;

exports.login = login;