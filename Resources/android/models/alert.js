var AlertModel = new joli.model({
    table: "alerts",
    columns: {
        id: "INTEGER PRIMARY KEY",
        name: "TEXT",
        zone: "INTEGER",
        alert_type: "INTEGER",
        created: "TEXT"
    },
    methods: {
        fetch: function(success, error) {
            var routeTimeout = setTimeout(function() {
                if (!finished) {
                    Ti.API.error("timeout exception");
                    error && error();
                }
            }, 45e3);
            var that = this;
            var finished = false;
            var client = Ti.Network.createHTTPClient({
                onload: function() {
                    that.truncate();
                    Ti.API.error("routes fetched");
                    clearTimeout(routeTimeout);
                    var data = JSON.parse(this.responseText);
                    _(data.objects).each(function(alertData) {
                        that.createRecord(alertData);
                    });
                    data.objects.length > 0 && Ti.App.Properties.setInt("lastAlert", data.objects[data.objects.length - 1].id);
                    Ti.App.fireEvent("alerts.fetched");
                    success && success();
                },
                onerror: function() {
                    clearTimeout(routeTimeout);
                    error && error();
                }
            });
            Ti.API.error(JSON.stringify(APP));
            Ti.API.error(JSON.stringify(APP.URL));
            client.setTimeout(3e4);
            client.open("GET", APP.URL + "alert_list.php?last=0");
            client.send();
        },
        createRecord: function(data) {
            Ti.API.info("CreateRecord route********************************");
            new joli.query().insertReplace("alerts").values({
                id: data.id,
                name: data.name,
                zone: data.zone,
                alert_type: data.alert_type,
                created: data.created
            }).execute();
            return data;
        },
        isEmpty: function() {
            return 0 === this.count();
        },
        getAll: function() {
            var query = new joli.query().select().from("alerts").order("created desc").limit("50").execute();
            return query;
        }
    },
    objectMethods: {}
});

module.exports = AlertModel;