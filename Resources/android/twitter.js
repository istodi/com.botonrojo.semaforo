var exports = exports || this;

exports.Twitter = function() {
    function createAuthWindow() {
        var self = this, oauth = this.oauthClient, webViewWindow = Ti.UI.createWindow({
            title: this.windowTitle
        }), webView = Ti.UI.createWebView(), loadingOverlay = Ti.UI.createView({
            backgroundColor: "black",
            opacity: .7,
            zIndex: 1
        }), actInd = Titanium.UI.createActivityIndicator({
            height: 50,
            width: 10,
            message: "Loading...",
            color: "white"
        }), closeButton = Ti.UI.createButton({
            title: "Close"
        }), backButton = Ti.UI.createButton({
            title: "Back"
        });
        this.webView = webView;
        webViewWindow.leftNavButton = closeButton;
        actInd.show();
        loadingOverlay.add(actInd);
        webViewWindow.add(loadingOverlay);
        webViewWindow.open({
            modal: true
        });
        webViewWindow.add(webView);
        closeButton.addEventListener("click", function() {
            webViewWindow.close();
            self.fireEvent("cancel", {
                success: false,
                error: "The user cancelled.",
                result: null
            });
        });
        backButton.addEventListener("click", function() {
            webView.goBack();
        });
        webView.addEventListener("beforeload", function() {
            isAndroid || webViewWindow.add(loadingOverlay);
            actInd.show();
        });
        webView.addEventListener("load", function(event) {
            if (-1 === event.url.indexOf(self.authorizeUrl)) {
                webViewWindow.remove(loadingOverlay);
                actInd.hide();
                webViewWindow.leftNavButton !== backButton && (webViewWindow.leftNavButton = backButton);
            } else {
                webViewWindow.leftNavButton !== closeButton && (webViewWindow.leftNavButton = closeButton);
                var pin = event.source.evalJS("document.getElementById('oauth_pin').getElementsByTagName('code')[0].innerText");
                if (pin) {
                    isAndroid || webViewWindow.close();
                    oauth.accessTokenUrl = "https://api.twitter.com/oauth/access_token?oauth_verifier=" + pin;
                    oauth.fetchAccessToken(function(data) {
                        var returnedParams = oauth.parseTokenRequest(data.text);
                        self.fireEvent("login", {
                            success: true,
                            error: false,
                            accessTokenKey: returnedParams.oauth_token,
                            accessTokenSecret: returnedParams.oauth_token_secret
                        });
                        isAndroid && webViewWindow.close();
                    }, function(data) {
                        self.fireEvent("login", {
                            success: false,
                            error: "Failure to fetch access token, please try again.",
                            result: data
                        });
                    });
                } else {
                    webViewWindow.remove(loadingOverlay);
                    actInd.hide();
                }
            }
        });
    }
    var K = function() {}, isAndroid = true, jsOAuth = require("jsOAuth-1.3.1");
    var Twitter = function(options) {
        var self;
        self = this instanceof Twitter ? this : new K();
        options || (options = {});
        self.windowTitle = options.windowTitle || "Twitter Authorization";
        self.consumerKey = options.consumerKey;
        self.consumerSecret = options.consumerSecret;
        self.authorizeUrl = "https://api.twitter.com/oauth/authorize";
        self.accessTokenKey = options.accessTokenKey;
        self.accessTokenSecret = options.accessTokenSecret;
        self.authorized = false;
        self.listeners = {};
        self.accessTokenKey && self.accessTokenSecret && (self.authorized = true);
        options.requestTokenUrl = options.requestTokenUrl || "https://api.twitter.com/oauth/request_token";
        self.oauthClient = jsOAuth.OAuth(options);
        return self;
    };
    K.prototype = Twitter.prototype;
    Twitter.prototype.authorize = function() {
        var self = this;
        if (this.authorized) setTimeout(function() {
            self.fireEvent("login", {
                success: true,
                error: false,
                accessTokenKey: self.accessTokenKey,
                accessTokenSecret: self.accessTokenSecret
            });
        }, 1); else {
            createAuthWindow.call(this);
            this.oauthClient.fetchRequestToken(function(requestParams) {
                var authorizeUrl = self.authorizeUrl + requestParams;
                self.webView.url = authorizeUrl;
            }, function(data) {
                self.fireEvent("login", {
                    success: false,
                    error: "Failure to fetch access token, please try again.",
                    result: data
                });
            });
        }
    };
    Twitter.prototype.request = function(path, params, httpVerb, callback) {
        var self = this, oauth = this.oauthClient, url = "https://api.twitter.com/" + path;
        oauth.request({
            method: httpVerb,
            url: url,
            data: params,
            success: function(data) {
                callback.call(self, {
                    success: true,
                    error: false,
                    result: data
                });
            },
            failure: function(data) {
                callback.call(self, {
                    success: false,
                    error: "Request failed",
                    result: data
                });
            }
        });
    };
    Twitter.prototype.addEventListener = function(eventName, callback) {
        this.listeners = this.listeners || {};
        this.listeners[eventName] = this.listeners[eventName] || [];
        this.listeners[eventName].push(callback);
    };
    Twitter.prototype.fireEvent = function(eventName, data) {
        var eventListeners = this.listeners[eventName] || [];
        for (var i = 0; eventListeners.length > i; i++) eventListeners[i].call(this, data);
    };
    return Twitter;
}(this);