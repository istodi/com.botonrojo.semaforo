var UTIL = require("utilities");

var AlertModel = require("models/alert");

var gcm = require("net.iamyellow.gcmjs");

var APP = {
    ID: null,
    VERSION: null,
    APPVERSION: null,
    DEVICETOKEN: null,
    COPY: null,
    URL: "http://192.168.0.100/",
    Nodes: [ {
        title: "Configurar alertas",
        type: "config"
    }, {
        title: "Tipos de alertas",
        type: "help"
    }, {
        title: "Términos y condiciones",
        type: "terms"
    }, {
        title: "Cerrar sesión",
        type: "close"
    } ],
    Settings: null,
    Device: {
        isHandheld: Alloy.isHandheld,
        isTablet: Alloy.isTablet,
        type: Alloy.isHandheld ? "handheld" : "tablet",
        os: null,
        name: null,
        version: Ti.Platform.version,
        versionMajor: parseInt(Ti.Platform.version.split(".")[0], 10),
        versionMinor: parseInt(Ti.Platform.version.split(".")[1], 10),
        width: Ti.Platform.displayCaps.platformWidth > Ti.Platform.displayCaps.platformHeight ? Ti.Platform.displayCaps.platformHeight : Ti.Platform.displayCaps.platformWidth,
        height: Ti.Platform.displayCaps.platformWidth > Ti.Platform.displayCaps.platformHeight ? Ti.Platform.displayCaps.platformWidth : Ti.Platform.displayCaps.platformHeight,
        dpi: Ti.Platform.displayCaps.dpi,
        orientation: Ti.Gesture.orientation == Ti.UI.LANDSCAPE_LEFT || Ti.Gesture.orientation == Ti.UI.LANDSCAPE_RIGHT ? "LANDSCAPE" : "PORTRAIT",
        statusBarOrientation: null
    },
    Network: {
        type: Ti.Network.networkTypeName,
        online: Ti.Network.online
    },
    currentStack: -1,
    previousScreen: null,
    controllerStacks: [],
    modalStack: [],
    hasDetail: false,
    currentDetailStack: -1,
    previousDetailScreen: null,
    detailStacks: [],
    Master: [],
    Detail: [],
    MainWindow: null,
    GlobalWrapper: null,
    ContentWrapper: null,
    mapView: null,
    Loading: Alloy.createWidget("com.kliento.loading"),
    cancelLoading: false,
    loadingOpen: false,
    Tabs: null,
    SlideMenu: null,
    SlideMenuOpen: false,
    SlideMenuEngaged: true,
    LoggedIn: Ti.App.Properties.getBool("loggedIn", false),
    init: function() {
        Ti.API.error("APP.init");
        APP.MainWindow.addEventListener("androidback", APP.backButtonObserver);
        APP.determineDevice();
        APP.setupDatabase();
        APP.build();
    },
    determineDevice: function() {
        APP.Device.os = "ANDROID";
        APP.Device.name = Ti.Platform.model.toUpperCase();
        APP.Device.width = APP.Device.width / (APP.Device.dpi / 160);
        APP.Device.height = APP.Device.height / (APP.Device.dpi / 160);
    },
    setupDatabase: function() {
        Ti.API.error("APP.setupDatabase");
        joli.models.initialize();
    },
    dropDatabase: function() {
        APP.log("error", "APP.dropDatabase");
        var models = [ "answers", "catalogs", "catalogsitems", "choices", "conditionals", "errorPing", "questions", "responses", "routes", "route_events", "route_points", "route_responses", "sections", "surveys", "update" ];
        _(models).each(function(modelName) {
            var query = "DROP TABLE IF EXISTS `" + modelName + "`";
            joli.connection.execute(query);
        });
    },
    build: function() {
        APP.log("error", "APP.build");
        APP.MainWindow.open();
        if (APP.LoggedIn) {
            var pendingData = gcm.data;
            if (pendingData && null !== pendingData) {
                APP.addChild("list", {}, false);
                APP.addChild("alert", pendingData, true);
            } else {
                var SYNC = require("sync");
                SYNC.fetch(function() {
                    APP.addChild("list", {}, false);
                });
            }
            APP.checkNotifications();
        } else {
            APP.log("error", "Login window");
            APP.addChild("login", {}, false);
        }
    },
    checkNotifications: function() {
        Ti.API.error("notification");
        gcm.registerForPushNotifications({
            success: function(ev) {
                Ti.API.info("******* success, " + ev.deviceToken);
                var client = Ti.Network.createHTTPClient({
                    onload: function() {
                        Ti.API.error("Dispositivo registrado", ev.deviceToken, Ti.Platform.id);
                    },
                    onerror: function() {
                        alert("No se puede conectar al servicio");
                    }
                });
                client.setTimeout(3e4);
                client.open("POST", APP.URL + "app_device_token.php");
                client.send(JSON.stringify({
                    id: Ti.App.Properties.getInt("userId"),
                    token: ev.deviceToken,
                    deviceId: Ti.Platform.id
                }));
            },
            error: function(ev) {
                alert("Notificaciones no disponibles");
                Ti.API.error("******* error, " + ev.error);
            },
            callback: function(data) {
                Ti.API.error(JSON.stringify(data));
                AlertModel.createRecord(data);
                Ti.App.Properties.setInt("lastAlert", data.id);
                Ti.App.fireEvent("alerts.fetched");
            },
            unregister: function(ev) {
                Ti.API.error("******* unregister, " + ev.deviceToken);
            },
            data: function(data) {
                APP.addChild("alert", data, true);
                Ti.API.error("******* data (resumed) " + JSON.stringify(data));
            }
        });
    },
    buildMenu: function(_nodes) {
        APP.log("debug", "APP.buildMenu");
        APP.SlideMenu.init({
            nodes: _nodes,
            color: {
                headingBackground: "#e6e6e6",
                headingText: "#000"
            }
        });
        APP.SlideMenu.Nodes.removeEventListener("click", APP.handleMenuClick);
        APP.SlideMenu.Nodes.addEventListener("click", APP.handleMenuClick);
    },
    handleTabClick: function(_event) {
        "undefined" != typeof _event.source.id && "number" == typeof _event.source.id && APP.handleNavigation(_event.source.id);
    },
    handleMenuClick: function(_event) {
        if ("undefined" != typeof _event.row.id && "number" == typeof _event.row.id) {
            APP.closeSettings();
            APP.handleNavigation(_event.row.id);
        } else "undefined" != typeof _event.row.id && "settings" == _event.row.id ? APP.openSettings() : "undefined" != typeof _event.row.id && "exit" == _event.row.id && dialogs.confirm({
            title: "Cerrar Aplicación",
            message: "¿Está seguro que desea salir de la aplicación? Con esto perderá comunicación con la empresa",
            callback: function() {
                Alloy.createWidget("com.mcongrove.toast", null, {
                    text: "Cerrando aplicación",
                    duration: 2e3,
                    view: APP.GlobalWrapper
                });
                Ti.App.fireEvent("app.exit");
                APP.exitApp();
            }
        });
        APP.toggleMenu();
    },
    addChild: function(_controller, _params, _sibling) {
        var screen = Alloy.createController(_controller, _params).getView();
        if (!_sibling) {
            APP.removeAllChildren();
            APP.controllerStacks = [];
        }
        APP.controllerStacks.push(screen);
        APP.addScreen(screen);
    },
    removeChild: function() {
        APP.removeScreen(APP.controllerStacks.pop());
    },
    removeAllChildren: function() {
        APP.controllerStacks = [];
        APP.GlobalWrapper.removeAllChildren();
    },
    addScreen: function(_screen) {
        _screen && APP.GlobalWrapper.add(_screen);
    },
    removeScreen: function(_screen) {
        _screen && APP.GlobalWrapper.remove(_screen);
    },
    openSettings: function() {
        APP.log("error", "APP.openSettings");
        APP.addChild("settings", {}, true);
    },
    closeSettings: function() {
        APP.modalStack.length > 0 && APP.removeChild(true);
    },
    toggleMenu: function() {
        Ti.API.error("toggle menu");
        APP.SlideMenuOpen ? APP.closeMenu() : APP.openMenu();
    },
    openMenu: function() {
        Ti.API.error("open menu");
        APP.addScreen(APP.SlideMenu.Wrapper);
        APP.SlideMenuOpen = true;
    },
    closeMenu: function() {
        Ti.API.error("close menu");
        APP.removeScreen(APP.SlideMenu.Wrapper);
        APP.SlideMenuOpen = false;
    },
    openLoading: function(message) {
        APP.cancelLoading = false;
        if (!APP.cancelLoading) {
            APP.loadingOpen = true;
            APP.Loading.init(message);
            APP.MainWindow.add(APP.Loading.getView());
        }
    },
    closeLoading: function() {
        APP.cancelLoading = true;
        if (APP.loadingOpen) {
            APP.MainWindow.remove(APP.Loading.getView());
            APP.loadingOpen = false;
        }
    },
    log: function(_severity, _text) {
        switch (_severity.toLowerCase()) {
          case "debug":
            Ti.API.debug(_text);
            break;

          case "error":
            Ti.API.error(_text);
            break;

          case "info":
            Ti.API.info(_text);
            break;

          case "log":
            Ti.API.log(_text);
            break;

          case "trace":
            Ti.API.trace(_text);
            break;

          case "warn":
            Ti.API.warn(_text);
        }
    },
    orientationObserver: function(_event) {
        APP.log("debug", "APP.orientationObserver");
        if (APP.Device.statusBarOrientation && APP.Device.statusBarOrientation == _event.orientation) return;
        APP.Device.statusBarOrientation = _event.orientation;
        APP.Device.orientation = _event.orientation == Ti.UI.LANDSCAPE_LEFT || _event.orientation == Ti.UI.LANDSCAPE_RIGHT ? "LANDSCAPE" : "PORTRAIT";
        Ti.App.fireEvent("APP:orientationChange");
    },
    backButtonObserver: function() {
        APP.log("error", "APP.backButtonObserver");
        if (APP.loadingOpen || APP.alertIsOpen) return false;
        var stack = APP.controllerStacks;
        if (stack.length > 1) APP.removeChild(); else {
            var intent = Ti.Android.createIntent({
                action: Ti.Android.ACTION_MAIN
            });
            intent.addCategory(Ti.Android.CATEGORY_HOME);
            Ti.Android.currentActivity.startActivity(intent);
        }
    }
};

module.exports = APP;