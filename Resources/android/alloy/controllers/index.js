function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.MainWindow = Ti.UI.createWindow({
        backgroundColor: "#FFF",
        navBarHidden: true,
        exitOnClose: true,
        id: "MainWindow"
    });
    $.__views.MainWindow && $.addTopLevelView($.__views.MainWindow);
    $.__views.GlobalWrapper = Ti.UI.createView({
        width: Ti.UI.FILL,
        id: "GlobalWrapper"
    });
    $.__views.MainWindow.add($.__views.GlobalWrapper);
    exports.destroy = function() {};
    _.extend($, $.__views);
    APP.MainWindow = $.MainWindow;
    APP.GlobalWrapper = $.GlobalWrapper;
    Ti.Platform.batteryMonitoring = true;
    APP.init();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;