function Controller() {
    function loginTW() {
        twitterClient.authorize();
    }
    function loginFB() {
        fb.authorize();
    }
    function loginMail() {
        APP.addChild("login_mail", {}, true);
    }
    function switchOrientation(orientation) {
        if (2 === orientation) {
            $.logo.applyProperties({
                top: "22%",
                left: "2%",
                width: "40%",
                height: Ti.UI.SIZE
            });
            $.buttonContainer.applyProperties({
                top: "20%",
                left: "45%",
                right: "2%",
                width: Ti.UI.FILL
            });
        } else if (1 === orientation) {
            $.logo.applyProperties({
                top: "7%",
                left: "auto",
                height: "21%",
                width: Ti.UI.SIZE
            });
            $.buttonContainer.applyProperties({
                top: "30%",
                left: "12.5%",
                right: "12.5%",
                width: "75%"
            });
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "login";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.loginView = Ti.UI.createView({
        width: Ti.UI.FILL,
        height: Ti.UI.FILL,
        top: 0,
        id: "loginView"
    });
    $.__views.loginView && $.addTopLevelView($.__views.loginView);
    $.__views.logo = Ti.UI.createImageView({
        height: "21%",
        top: "7%",
        center: {
            x: "50%",
            y: "50%"
        },
        image: "/images/logo.png",
        touchEnabled: false,
        id: "logo"
    });
    $.__views.loginView.add($.__views.logo);
    $.__views.buttonContainer = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.FILL,
        top: "30%",
        bottom: "14%",
        width: "75%",
        id: "buttonContainer"
    });
    $.__views.loginView.add($.__views.buttonContainer);
    $.__views.loginTw = Ti.UI.createView({
        bottom: "10dp",
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        id: "loginTw",
        backgroundColor: "#2ba7de"
    });
    $.__views.buttonContainer.add($.__views.loginTw);
    loginTW ? $.__views.loginTw.addEventListener("click", loginTW) : __defers["$.__views.loginTw!click!loginTW"] = true;
    $.__views.__alloyId27 = Ti.UI.createLabel({
        color: "#FFF",
        text: "Conectar con Twitter",
        top: "10dp",
        bottom: "10dp",
        id: "__alloyId27"
    });
    $.__views.loginTw.add($.__views.__alloyId27);
    $.__views.loginFB = Ti.UI.createView({
        bottom: "10dp",
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        id: "loginFB",
        backgroundColor: "#2e5291"
    });
    $.__views.buttonContainer.add($.__views.loginFB);
    loginFB ? $.__views.loginFB.addEventListener("click", loginFB) : __defers["$.__views.loginFB!click!loginFB"] = true;
    $.__views.__alloyId28 = Ti.UI.createLabel({
        color: "#FFF",
        text: "Conectar con Facebook",
        top: "10dp",
        bottom: "10dp",
        id: "__alloyId28"
    });
    $.__views.loginFB.add($.__views.__alloyId28);
    $.__views.loginMail = Ti.UI.createView({
        bottom: "10dp",
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        id: "loginMail",
        backgroundColor: "#4d4d4d"
    });
    $.__views.buttonContainer.add($.__views.loginMail);
    loginMail ? $.__views.loginMail.addEventListener("click", loginMail) : __defers["$.__views.loginMail!click!loginMail"] = true;
    $.__views.__alloyId29 = Ti.UI.createLabel({
        color: "#FFF",
        text: "Conectar con correo electrónico",
        top: "10dp",
        bottom: "10dp",
        id: "__alloyId29"
    });
    $.__views.loginMail.add($.__views.__alloyId29);
    $.__views.footer = Ti.UI.createView({
        height: "14%",
        bottom: "1dp",
        left: "5dp",
        right: "5dp",
        width: Ti.UI.FILL,
        id: "footer"
    });
    $.__views.loginView.add($.__views.footer);
    $.__views.logoReynosa = Ti.UI.createImageView({
        left: "10dp",
        right: "60%",
        height: Ti.UI.FILL,
        width: Ti.UI.SIZE,
        center: {
            x: "50%",
            y: "50%"
        },
        image: "/images/logoReynosa.png",
        id: "logoReynosa"
    });
    $.__views.footer.add($.__views.logoReynosa);
    $.__views.developedBy = Ti.UI.createLabel({
        color: "#000",
        width: "40%",
        right: 0,
        id: "developedBy",
        text: "Desarrollo por Botón Rojo",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
    });
    $.__views.footer.add($.__views.developedBy);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var SYNC = require("sync");
    var twitterClient = Twitter({
        consumerKey: "OIlsfXD7zprIVGiTWepA4znNy",
        consumerSecret: "a8K2anLfprlmwuUFxR2iisOyczSPdZTfp9ECXA49AqFvSxSxwj"
    });
    twitterClient.addEventListener("login", function(e) {
        e.success ? twitterClient.request("1.1/account/verify_credentials.json", {}, "GET", function(e) {
            if (e.success) {
                var jsonData = JSON.parse(e.result.text);
                var data = {
                    mail: "",
                    pass: "",
                    name: jsonData.name,
                    login_type: "1",
                    twitter_username: jsonData.screen_name
                };
                SYNC.login(data);
            } else alert(e.error);
        }) : alert(e.error);
    });
    fb.appid = 619173798190450;
    fb.permissions = [ "email", "public_profile" ];
    fb.addEventListener("login", function(e) {
        e.success ? fb.requestWithGraphPath("me", {}, "GET", function(e) {
            if (e.success) {
                var responseData = JSON.parse(e.result);
                var data = {
                    mail: responseData.email,
                    pass: "",
                    name: (responseData.first_name + " " + responseData.last_name).trim(),
                    login_type: "2",
                    twitter_username: ""
                };
                SYNC.login(data);
            } else e.error ? alert(e.error) : alert("Unknown response");
        }) : e.error && alert(e.error);
    });
    $.init = function() {
        $.loginView.backgroundImage = "/images/bg.png";
    };
    Ti.Gesture.addEventListener("orientationchange", function() {
        switchOrientation(Titanium.Gesture.orientation);
    });
    switchOrientation(Titanium.Gesture.orientation);
    $.init();
    __defers["$.__views.loginTw!click!loginTW"] && $.__views.loginTw.addEventListener("click", loginTW);
    __defers["$.__views.loginFB!click!loginFB"] && $.__views.loginFB.addEventListener("click", loginFB);
    __defers["$.__views.loginMail!click!loginMail"] && $.__views.loginMail.addEventListener("click", loginMail);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;