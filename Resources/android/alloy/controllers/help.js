function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "help";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.Wrapper = Ti.UI.createView({
        id: "Wrapper",
        name: "Settings"
    });
    $.__views.Wrapper && $.addTopLevelView($.__views.Wrapper);
    $.__views.NavigationBar = Alloy.createWidget("com.mcongrove.navigationBar", "widget", {
        id: "NavigationBar",
        text: "SEMÁFORO REYNOSA",
        __parentSymbol: $.__views.Wrapper
    });
    $.__views.NavigationBar.setParent($.__views.Wrapper);
    $.__views.wrapperContainer = Ti.UI.createView({
        top: "47dp",
        layout: "vertical",
        width: Ti.UI.FILL,
        backgroundColor: "#FFF",
        id: "wrapperContainer"
    });
    $.__views.Wrapper.add($.__views.wrapperContainer);
    $.__views.header = Ti.UI.createView({
        backgroundColor: "#4D4D4D",
        height: "65dp",
        id: "header"
    });
    $.__views.wrapperContainer.add($.__views.header);
    $.__views.headerText = Ti.UI.createLabel({
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        color: "#FFF",
        width: "75%",
        font: {
            fontSize: "20dp",
            fontWeight: "bold"
        },
        id: "headerText",
        text: "¿Qué significan los colores de las alertas?"
    });
    $.__views.header.add($.__views.headerText);
    var __alloyId9 = [];
    $.__views.__alloyId10 = Ti.UI.createTableViewSection({
        id: "__alloyId10"
    });
    __alloyId9.push($.__views.__alloyId10);
    $.__views.__alloyId11 = Ti.UI.createTableViewRow({
        id: "__alloyId11"
    });
    $.__views.__alloyId10.add($.__views.__alloyId11);
    $.__views.__alloyId12 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        id: "__alloyId12"
    });
    $.__views.__alloyId11.add($.__views.__alloyId12);
    $.__views.__alloyId13 = Ti.UI.createImageView({
        top: "20dp",
        left: "13dp",
        image: "/images/alert1.png",
        width: "40dp",
        height: "40dp",
        id: "__alloyId13"
    });
    $.__views.__alloyId12.add($.__views.__alloyId13);
    $.__views.__alloyId14 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        layout: "vertical",
        left: "63dp",
        right: "10dp",
        top: "10dp",
        bottom: "10dp",
        id: "__alloyId14"
    });
    $.__views.__alloyId12.add($.__views.__alloyId14);
    $.__views.title = Ti.UI.createLabel({
        id: "title",
        text: "VERDE",
        color: "#049244",
        left: "0"
    });
    $.__views.__alloyId14.add($.__views.title);
    $.__views.description = Ti.UI.createLabel({
        id: "description",
        text: "Personal trabajando en vialidades o accidente vehicular.",
        color: "#000",
        left: "0"
    });
    $.__views.__alloyId14.add($.__views.description);
    $.__views.__alloyId15 = Ti.UI.createTableViewRow({
        id: "__alloyId15"
    });
    $.__views.__alloyId10.add($.__views.__alloyId15);
    $.__views.__alloyId16 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        id: "__alloyId16"
    });
    $.__views.__alloyId15.add($.__views.__alloyId16);
    $.__views.__alloyId17 = Ti.UI.createImageView({
        top: "20dp",
        left: "13dp",
        image: "/images/alert2.png",
        width: "40dp",
        height: "40dp",
        id: "__alloyId17"
    });
    $.__views.__alloyId16.add($.__views.__alloyId17);
    $.__views.__alloyId18 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        layout: "vertical",
        left: "63dp",
        right: "10dp",
        top: "10dp",
        bottom: "10dp",
        id: "__alloyId18"
    });
    $.__views.__alloyId16.add($.__views.__alloyId18);
    $.__views.title = Ti.UI.createLabel({
        id: "title",
        text: "AMARILLO",
        color: "#faa530",
        left: "0"
    });
    $.__views.__alloyId18.add($.__views.title);
    $.__views.description = Ti.UI.createLabel({
        id: "description",
        text: "Vialidad cerrada o información (sin confirmar) sobre posible contingencia en un sector de la ciudad.",
        color: "#000",
        left: "0"
    });
    $.__views.__alloyId18.add($.__views.description);
    $.__views.__alloyId19 = Ti.UI.createTableViewRow({
        id: "__alloyId19"
    });
    $.__views.__alloyId10.add($.__views.__alloyId19);
    $.__views.__alloyId20 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        id: "__alloyId20"
    });
    $.__views.__alloyId19.add($.__views.__alloyId20);
    $.__views.__alloyId21 = Ti.UI.createImageView({
        top: "20dp",
        left: "13dp",
        image: "/images/alert3.png",
        width: "40dp",
        height: "40dp",
        id: "__alloyId21"
    });
    $.__views.__alloyId20.add($.__views.__alloyId21);
    $.__views.__alloyId22 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        layout: "vertical",
        left: "63dp",
        right: "10dp",
        top: "10dp",
        bottom: "10dp",
        id: "__alloyId22"
    });
    $.__views.__alloyId20.add($.__views.__alloyId22);
    $.__views.title = Ti.UI.createLabel({
        id: "title",
        text: "NARANJA",
        color: "#EF5A22",
        left: "0"
    });
    $.__views.__alloyId22.add($.__views.title);
    $.__views.description = Ti.UI.createLabel({
        id: "description",
        text: "Contingencia confirmada en un sector de la ciudad.",
        color: "#000",
        left: "0"
    });
    $.__views.__alloyId22.add($.__views.description);
    $.__views.__alloyId23 = Ti.UI.createTableViewRow({
        id: "__alloyId23"
    });
    $.__views.__alloyId10.add($.__views.__alloyId23);
    $.__views.__alloyId24 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        id: "__alloyId24"
    });
    $.__views.__alloyId23.add($.__views.__alloyId24);
    $.__views.__alloyId25 = Ti.UI.createImageView({
        top: "20dp",
        left: "13dp",
        image: "/images/alert4.png",
        width: "40dp",
        height: "40dp",
        id: "__alloyId25"
    });
    $.__views.__alloyId24.add($.__views.__alloyId25);
    $.__views.__alloyId26 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        layout: "vertical",
        left: "63dp",
        right: "10dp",
        top: "10dp",
        bottom: "10dp",
        id: "__alloyId26"
    });
    $.__views.__alloyId24.add($.__views.__alloyId26);
    $.__views.title = Ti.UI.createLabel({
        id: "title",
        text: "ROJO",
        color: "#C0272A",
        left: "0"
    });
    $.__views.__alloyId26.add($.__views.title);
    $.__views.description = Ti.UI.createLabel({
        id: "description",
        text: "Contingencias en distintos puntos de la ciudad o fuerte contingencia en un sector.",
        color: "#000",
        left: "0"
    });
    $.__views.__alloyId26.add($.__views.description);
    $.__views.table = Ti.UI.createTableView({
        separatorColor: "#cccccc",
        showVerticalScrollIndicator: true,
        data: __alloyId9,
        id: "table"
    });
    $.__views.wrapperContainer.add($.__views.table);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.init = function() {
        $.NavigationBar.showMenu(function(_event) {
            _event.cancelBubble = true;
            $.NavigationBar.toggleMenu();
        });
        $.NavigationBar.showBack(function(_event) {
            _event.cancelBubble = true;
            APP.removeChild();
        });
    };
    $.init();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;