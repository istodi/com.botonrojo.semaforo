function Controller() {
    function showList() {
        SYNC.fetch(function() {
            APP.removeChild();
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "alert";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.Wrapper = Ti.UI.createView({
        id: "Wrapper",
        name: "Alert detail"
    });
    $.__views.Wrapper && $.addTopLevelView($.__views.Wrapper);
    $.__views.NavigationBar = Alloy.createWidget("com.mcongrove.navigationBar", "widget", {
        id: "NavigationBar",
        text: "SEMÁFORO REYNOSA",
        __parentSymbol: $.__views.Wrapper
    });
    $.__views.NavigationBar.setParent($.__views.Wrapper);
    $.__views.container = Ti.UI.createScrollView({
        top: "47dp",
        backgroundColor: "#FFF",
        layout: "vertical",
        showVerticalScrollIndicator: true,
        id: "container"
    });
    $.__views.Wrapper.add($.__views.container);
    $.__views.alertLogo = Ti.UI.createImageView({
        id: "alertLogo",
        top: "30dp",
        width: "30%"
    });
    $.__views.container.add($.__views.alertLogo);
    $.__views.message = Ti.UI.createLabel({
        color: "#FFF",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        font: {
            fontSize: "20dp"
        },
        width: "75%",
        id: "message",
        top: "20dp"
    });
    $.__views.container.add($.__views.message);
    $.__views.created = Ti.UI.createLabel({
        color: "#FFF",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        id: "created",
        top: "10dp"
    });
    $.__views.container.add($.__views.created);
    $.__views.save = Ti.UI.createView({
        id: "save",
        backgroundColor: "#FFFF",
        top: "30dp",
        width: "50%",
        height: Ti.UI.SIZE,
        bottom: "10dp"
    });
    $.__views.container.add($.__views.save);
    showList ? $.__views.save.addEventListener("click", showList) : __defers["$.__views.save!click!showList"] = true;
    $.__views.__alloyId5 = Ti.UI.createLabel({
        color: "#000",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "VER MÁS",
        top: "5dp",
        bottom: "5dp",
        id: "__alloyId5"
    });
    $.__views.save.add($.__views.__alloyId5);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var SYNC = require("sync");
    var CONFIG = arguments[0];
    var BACKGROUNDS = [ "", "#049244", "#faa530", "#EF5A22", "#C0272A" ];
    $.init = function() {
        APP.log("error", "alert.detail.init");
        var alert_type = parseInt(CONFIG.alert_type, 10);
        $.container.setBackgroundColor(BACKGROUNDS[alert_type]);
        $.alertLogo.setImage("/images/alert_" + alert_type + ".png");
        $.message.setText(CONFIG.message);
        $.created.setText(moment(CONFIG.created).format("dddd DD/MM/YYYY - h:mm A"));
    };
    $.init();
    __defers["$.__views.save!click!showList"] && $.__views.save.addEventListener("click", showList);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;