function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "row";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.Wrapper = Ti.UI.createTableViewRow({
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        backgroundSelectedColor: "#EEE",
        backgroundColor: "#FFF",
        id: "Wrapper"
    });
    $.__views.Wrapper && $.addTopLevelView($.__views.Wrapper);
    $.__views.border = Ti.UI.createView({
        width: "3dp",
        left: 0,
        id: "border"
    });
    $.__views.Wrapper.add($.__views.border);
    $.__views.alertIcon = Ti.UI.createImageView({
        width: "40dp",
        height: "40dp",
        top: "20dp",
        left: "13dp",
        id: "alertIcon"
    });
    $.__views.Wrapper.add($.__views.alertIcon);
    $.__views.container = Ti.UI.createView({
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        layout: "vertical",
        left: "63dp",
        right: "10dp",
        top: "10dp",
        bottom: "10dp",
        id: "container"
    });
    $.__views.Wrapper.add($.__views.container);
    $.__views.created = Ti.UI.createLabel({
        left: 0,
        font: {
            fontSize: "12dp"
        },
        color: "#999999",
        id: "created"
    });
    $.__views.container.add($.__views.created);
    $.__views.name = Ti.UI.createLabel({
        left: 0,
        font: {
            fontSize: "16dp"
        },
        color: "#000",
        id: "name"
    });
    $.__views.container.add($.__views.name);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var CONFIG = arguments[0] || {};
    var row_1 = $.createStyle({
        classes: "row_1"
    });
    var row_2 = $.createStyle({
        classes: "row_2"
    });
    var row_3 = $.createStyle({
        classes: "row_3"
    });
    var row_4 = $.createStyle({
        classes: "row_4"
    });
    var alert_type = CONFIG.alert_type;
    switch (alert_type) {
      case 1:
        $.border.applyProperties(row_1);
        $.alertIcon.image = "/images/alert1.png";
        break;

      case 2:
        $.border.applyProperties(row_2);
        $.alertIcon.image = "/images/alert2.png";
        break;

      case 3:
        $.border.applyProperties(row_3);
        $.alertIcon.image = "/images/alert3.png";
        break;

      case 4:
        $.border.applyProperties(row_4);
        $.alertIcon.image = "/images/alert4.png";
    }
    $.name.text = CONFIG.name || "";
    $.created.text = moment(CONFIG.created).format("dddd DD/MM/YYYY - h:mm A");
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;