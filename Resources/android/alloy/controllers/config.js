function Controller() {
    function setChecked(args) {
        if ("#F1F1F1" === args.item.getBackgroundColor()) {
            Ti.App.Properties.setBool(args.notification, true);
            args.item.setBackgroundColor(args.backGroundColor);
            args.buttonCheck.setColor("#FFF");
            args.buttonTxt.setColor("#FFF");
            args.buttonCheck.text = "✔";
        } else {
            Ti.App.Properties.setBool(args.notification, false);
            args.item.setBackgroundColor("#F1F1F1");
            args.buttonCheck.setColor("#a3a3a3");
            args.buttonTxt.setColor("#a3a3a3");
            args.buttonCheck.text = "";
        }
    }
    function save() {
        CONFIG.hasOwnProperty("back") && !CONFIG.back ? SYNC.fetch(function() {
            APP.addChild("list", {}, false);
        }) : APP.removeChild();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "config";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.Wrapper = Ti.UI.createView({
        id: "Wrapper",
        name: "Settings"
    });
    $.__views.Wrapper && $.addTopLevelView($.__views.Wrapper);
    $.__views.NavigationBar = Alloy.createWidget("com.mcongrove.navigationBar", "widget", {
        id: "NavigationBar",
        text: "SEMÁFORO REYNOSA",
        __parentSymbol: $.__views.Wrapper
    });
    $.__views.NavigationBar.setParent($.__views.Wrapper);
    $.__views.wrapperContainer = Ti.UI.createView({
        top: "47dp",
        layout: "vertical",
        width: Ti.UI.FILL,
        backgroundColor: "#FFF",
        id: "wrapperContainer"
    });
    $.__views.Wrapper.add($.__views.wrapperContainer);
    $.__views.header = Ti.UI.createView({
        backgroundColor: "#4D4D4D",
        height: "65dp",
        id: "header"
    });
    $.__views.wrapperContainer.add($.__views.header);
    $.__views.headerText = Ti.UI.createLabel({
        font: {
            fontSize: "20dp",
            fontWeight: "bold"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        color: "#FFF",
        width: "75%",
        id: "headerText",
        text: "Elige las alertas que deseas recibir:"
    });
    $.__views.header.add($.__views.headerText);
    $.__views.container = Ti.UI.createScrollView({
        width: Ti.UI.FILL,
        backgroundColor: "#FFF",
        layout: "vertical",
        showVerticalScrollIndicator: true,
        id: "container"
    });
    $.__views.wrapperContainer.add($.__views.container);
    $.__views.__alloyId6 = Ti.UI.createLabel({
        font: {
            fontSize: "18dp"
        },
        top: "10dp",
        width: "75%",
        color: "#000",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        text: "Notificaciones",
        id: "__alloyId6"
    });
    $.__views.container.add($.__views.__alloyId6);
    $.__views.green = Ti.UI.createView({
        width: "75%",
        backgroundColor: "#049244",
        height: Ti.UI.SIZE,
        top: "5dp",
        id: "green"
    });
    $.__views.container.add($.__views.green);
    $.__views.alert1 = Ti.UI.createLabel({
        font: {
            fontSize: "16dp"
        },
        color: "#FFF",
        left: "10dp",
        top: "10dp",
        bottom: "10dp",
        id: "alert1",
        text: "Verde"
    });
    $.__views.green.add($.__views.alert1);
    $.__views.check1 = Ti.UI.createLabel({
        font: {
            fontSize: "16dp"
        },
        color: "#FFF",
        top: "10dp",
        right: "10dp",
        bottom: "10dp",
        id: "check1",
        text: "✔"
    });
    $.__views.green.add($.__views.check1);
    $.__views.yellow = Ti.UI.createView({
        width: "75%",
        backgroundColor: "#faa530",
        height: Ti.UI.SIZE,
        top: "5dp",
        id: "yellow"
    });
    $.__views.container.add($.__views.yellow);
    $.__views.alert2 = Ti.UI.createLabel({
        font: {
            fontSize: "16dp"
        },
        color: "#FFF",
        left: "10dp",
        top: "10dp",
        bottom: "10dp",
        id: "alert2",
        text: "Amarillo"
    });
    $.__views.yellow.add($.__views.alert2);
    $.__views.check2 = Ti.UI.createLabel({
        font: {
            fontSize: "16dp"
        },
        color: "#FFF",
        top: "10dp",
        right: "10dp",
        bottom: "10dp",
        id: "check2",
        text: "✔"
    });
    $.__views.yellow.add($.__views.check2);
    $.__views.orange = Ti.UI.createView({
        width: "75%",
        backgroundColor: "#EF5A22",
        height: Ti.UI.SIZE,
        top: "5dp",
        id: "orange"
    });
    $.__views.container.add($.__views.orange);
    $.__views.alert3 = Ti.UI.createLabel({
        font: {
            fontSize: "16dp"
        },
        color: "#FFF",
        left: "10dp",
        top: "10dp",
        bottom: "10dp",
        id: "alert3",
        text: "Naranja"
    });
    $.__views.orange.add($.__views.alert3);
    $.__views.check3 = Ti.UI.createLabel({
        font: {
            fontSize: "16dp"
        },
        color: "#FFF",
        top: "10dp",
        right: "10dp",
        bottom: "10dp",
        id: "check3",
        text: "✔"
    });
    $.__views.orange.add($.__views.check3);
    $.__views.red = Ti.UI.createView({
        width: "75%",
        backgroundColor: "#C0272A",
        height: Ti.UI.SIZE,
        top: "5dp",
        id: "red"
    });
    $.__views.container.add($.__views.red);
    $.__views.alert4 = Ti.UI.createLabel({
        font: {
            fontSize: "16dp"
        },
        color: "#FFF",
        left: "10dp",
        top: "10dp",
        bottom: "10dp",
        id: "alert4",
        text: "Rojo"
    });
    $.__views.red.add($.__views.alert4);
    $.__views.check4 = Ti.UI.createLabel({
        font: {
            fontSize: "16dp"
        },
        color: "#FFF",
        top: "10dp",
        right: "10dp",
        bottom: "10dp",
        id: "check4",
        text: "✔"
    });
    $.__views.red.add($.__views.check4);
    $.__views.__alloyId7 = Ti.UI.createLabel({
        font: {
            fontSize: "18dp"
        },
        top: "10dp",
        width: "75%",
        color: "#000",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        text: "Zonas",
        id: "__alloyId7"
    });
    $.__views.container.add($.__views.__alloyId7);
    $.__views.allZones = Ti.UI.createView({
        width: "75%",
        backgroundColor: "#4d4d4d",
        height: Ti.UI.SIZE,
        top: "5dp",
        id: "allZones"
    });
    $.__views.container.add($.__views.allZones);
    $.__views.allZonesTxt = Ti.UI.createLabel({
        font: {
            fontSize: "16dp"
        },
        color: "#FFF",
        left: "10dp",
        top: "10dp",
        bottom: "10dp",
        id: "allZonesTxt",
        text: "Todas"
    });
    $.__views.allZones.add($.__views.allZonesTxt);
    $.__views.allZonesCheck = Ti.UI.createLabel({
        font: {
            fontSize: "16dp"
        },
        color: "#FFF",
        top: "10dp",
        right: "10dp",
        bottom: "10dp",
        id: "allZonesCheck",
        text: "✔"
    });
    $.__views.allZones.add($.__views.allZonesCheck);
    $.__views.save = Ti.UI.createView({
        id: "save",
        backgroundColor: "#C0272A",
        top: "20dp",
        width: "60%",
        height: Ti.UI.SIZE,
        bottom: "20dp"
    });
    $.__views.container.add($.__views.save);
    save ? $.__views.save.addEventListener("click", save) : __defers["$.__views.save!click!save"] = true;
    $.__views.__alloyId8 = Ti.UI.createLabel({
        font: {
            fontSize: "16dp"
        },
        text: "VER SEMÁFORO",
        color: "#FFF",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        top: "10dp",
        bottom: "10dp",
        id: "__alloyId8"
    });
    $.__views.save.add($.__views.__alloyId8);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var SYNC = require("sync");
    var GREEN = "#049244";
    var YELLOW = "#faa530";
    var ORANGE = "#EF5A22";
    var RED = "#C0272A";
    var CONFIG = arguments[0];
    $.init = function() {
        Ti.API.error("show config");
        var alert1 = Ti.App.Properties.getBool("alert_1", true);
        if (!alert1) {
            $.green.setBackgroundColor("#F1F1F1");
            $.alert1.setColor("#a3a3a3");
            $.check1.text = "";
        }
        var alert2 = Ti.App.Properties.getBool("alert_2", true);
        if (!alert2) {
            $.yellow.setBackgroundColor("#F1F1F1");
            $.alert2.setColor("#a3a3a3");
            $.check2.text = "";
        }
        var alert3 = Ti.App.Properties.getBool("alert_3", true);
        if (!alert3) {
            $.orange.setBackgroundColor("#F1F1F1");
            $.alert3.setColor("#a3a3a3");
            $.check3.text = "";
        }
        var alert4 = Ti.App.Properties.getBool("alert_4", true);
        if (!alert4) {
            $.red.setBackgroundColor("#F1F1F1");
            $.alert4.setColor("#a3a3a3");
            $.check4.text = "";
        }
        (CONFIG.hasOwnProperty("menu") ? CONFIG.back : true) && $.NavigationBar.showMenu(function(_event) {
            _event.cancelBubble = true;
            $.NavigationBar.toggleMenu();
        });
        (CONFIG.hasOwnProperty("back") ? CONFIG.back : true) && $.NavigationBar.showBack(function(_event) {
            _event.cancelBubble = true;
            APP.removeChild();
        });
    };
    $.green.addEventListener("click", function() {
        setChecked({
            item: $.green,
            backGroundColor: GREEN,
            buttonTxt: $.alert1,
            buttonCheck: $.check1,
            notification: "alert_1"
        });
    });
    $.yellow.addEventListener("click", function() {
        setChecked({
            item: $.yellow,
            backGroundColor: YELLOW,
            buttonTxt: $.alert2,
            buttonCheck: $.check2,
            notification: "alert_2"
        });
    });
    $.orange.addEventListener("click", function() {
        setChecked({
            item: $.orange,
            backGroundColor: ORANGE,
            buttonTxt: $.alert3,
            buttonCheck: $.check3,
            notification: "alert_3"
        });
    });
    $.red.addEventListener("click", function() {
        setChecked({
            item: $.red,
            backGroundColor: RED,
            buttonTxt: $.alert4,
            buttonCheck: $.check4,
            notification: "alert_4"
        });
    });
    $.init();
    __defers["$.__views.save!click!save"] && $.__views.save.addEventListener("click", save);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;