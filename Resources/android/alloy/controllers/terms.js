function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "terms";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.Wrapper = Ti.UI.createView({
        width: Ti.UI.FILL,
        backgroundColor: "#FFF",
        id: "Wrapper"
    });
    $.__views.Wrapper && $.addTopLevelView($.__views.Wrapper);
    $.__views.NavigationBar = Alloy.createWidget("com.mcongrove.navigationBar", "widget", {
        id: "NavigationBar",
        text: "SEMÁFORO REYNOSA",
        __parentSymbol: $.__views.Wrapper
    });
    $.__views.NavigationBar.setParent($.__views.Wrapper);
    $.__views.container = Ti.UI.createScrollView({
        left: "5dp",
        right: "5dp",
        top: "52dp",
        layout: "vertical",
        showVerticalScrollIndicator: true,
        id: "container"
    });
    $.__views.Wrapper.add($.__views.container);
    $.__views.__alloyId33 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "RESPONSABLE DEL TRATAMIENTO DE DATOS PERSONALES",
        id: "__alloyId33"
    });
    $.__views.container.add($.__views.__alloyId33);
    $.__views.__alloyId34 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "El Gobierno de Reynosa es responsable del tratamiento de sus datos personales conforme a este aviso de privacidad.",
        id: "__alloyId34"
    });
    $.__views.container.add($.__views.__alloyId34);
    $.__views.__alloyId35 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "FINALIDAD DEL TRATAMIENTO DE LOS DATOS PERSONALES",
        id: "__alloyId35"
    });
    $.__views.container.add($.__views.__alloyId35);
    $.__views.__alloyId36 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "Los Datos Personales en posesión del Gobierno de Reynosa serán utilizados para:",
        id: "__alloyId36"
    });
    $.__views.container.add($.__views.__alloyId36);
    $.__views.__alloyId37 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        text: "- Prestación de servicios.",
        id: "__alloyId37"
    });
    $.__views.container.add($.__views.__alloyId37);
    $.__views.__alloyId38 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        text: "- Información sobre servicios o eventos incluyendo envío de publicidad y boletines informativos del Gobierno Municipal y cualquiera de sus dependencias.",
        id: "__alloyId38"
    });
    $.__views.container.add($.__views.__alloyId38);
    $.__views.__alloyId39 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        text: "- Dar cumplimiento a obligaciones.",
        id: "__alloyId39"
    });
    $.__views.container.add($.__views.__alloyId39);
    $.__views.__alloyId40 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        text: "- Evaluaciones de calidad y servicio.",
        id: "__alloyId40"
    });
    $.__views.container.add($.__views.__alloyId40);
    $.__views.__alloyId41 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        text: "- Análisis internos de servicios.",
        id: "__alloyId41"
    });
    $.__views.container.add($.__views.__alloyId41);
    $.__views.__alloyId42 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: '"Para cumplir con los fines anteriores requeriremos datos personales de identificación tales como nombre, edad y datos de contacto incluyendo teléfono, domicilio y cuenta de correo electrónico."',
        id: "__alloyId42"
    });
    $.__views.container.add($.__views.__alloyId42);
    $.__views.__alloyId43 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "TRANFERENCIA",
        id: "__alloyId43"
    });
    $.__views.container.add($.__views.__alloyId43);
    $.__views.__alloyId44 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "El Gobierno de Reynosa puede transferir los datos personales en su posesión a terceros subcontratados o relacionados con la prestación de servicios, con fines publicitarios o estudios de mercado.",
        id: "__alloyId44"
    });
    $.__views.container.add($.__views.__alloyId44);
    $.__views.__alloyId45 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "LIMITACIÓN DE USO Y DIVULGACIÓN DE DATOS PERSONALES",
        id: "__alloyId45"
    });
    $.__views.container.add($.__views.__alloyId45);
    $.__views.__alloyId46 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "Para limitar el uso de sus datos personales, favor de enviar un correo electrónico a semaforo@reynosa.gob.mx",
        id: "__alloyId46"
    });
    $.__views.container.add($.__views.__alloyId46);
    $.__views.__alloyId47 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "Se considera que autoriza el uso de sus datos personales conforme a este aviso de privacidad, salvo comunicación en contrario.",
        id: "__alloyId47"
    });
    $.__views.container.add($.__views.__alloyId47);
    $.__views.__alloyId48 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "MEDIOS PARA EJERCER DERECHOS ARCO (ACCESO, RECTIFICACIÓN, CANCELACIÓN Y OPOSICIÓN)",
        id: "__alloyId48"
    });
    $.__views.container.add($.__views.__alloyId48);
    $.__views.__alloyId49 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "Puede Acceder, Rectificar, Cancelar y Oponerse al uso de sus datos personales envie una solicitud por escrito a semaforo@reynosa.gob.mx y que contenga la siguiente información:",
        id: "__alloyId49"
    });
    $.__views.container.add($.__views.__alloyId49);
    $.__views.__alloyId50 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        text: "- Nombre completo.",
        id: "__alloyId50"
    });
    $.__views.container.add($.__views.__alloyId50);
    $.__views.__alloyId51 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        text: "- Dirección de correo electrónico para comunicar respuesta a solicitud.",
        id: "__alloyId51"
    });
    $.__views.container.add($.__views.__alloyId51);
    $.__views.__alloyId52 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        text: "- Documentos que acrediten identidad o autorización para representarlo en la solicitud.",
        id: "__alloyId52"
    });
    $.__views.container.add($.__views.__alloyId52);
    $.__views.__alloyId53 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        text: "- Descripción de datos personales sobre los que se pretende ejercer algún derecho ARCO.",
        id: "__alloyId53"
    });
    $.__views.container.add($.__views.__alloyId53);
    $.__views.__alloyId54 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        text: "- Cualquier otro elemento que permita la localización de los datos personales y atención a la solicitud.",
        id: "__alloyId54"
    });
    $.__views.container.add($.__views.__alloyId54);
    $.__views.__alloyId55 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "MEDIOS PARA REVOCAR CONSENTIMIENTO",
        id: "__alloyId55"
    });
    $.__views.container.add($.__views.__alloyId55);
    $.__views.__alloyId56 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "Puede revocar el consentimiento para tratar sus datos personales enviando una solicitud por escrito dirigida semaforo@reynosa.gob.mx en la que se detalle claramente los datos respecto de los que revoca su consentimiento.",
        id: "__alloyId56"
    });
    $.__views.container.add($.__views.__alloyId56);
    $.__views.__alloyId57 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "NOTIFICACIÓN DE CAMBIOS AL AVISO DE PRIVACIDAD",
        id: "__alloyId57"
    });
    $.__views.container.add($.__views.__alloyId57);
    $.__views.__alloyId58 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "En su caso, las modificaciones a este Aviso de Privacidad estarán disponibles en este mismo medio.",
        id: "__alloyId58"
    });
    $.__views.container.add($.__views.__alloyId58);
    $.__views.__alloyId59 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "AVISO",
        id: "__alloyId59"
    });
    $.__views.container.add($.__views.__alloyId59);
    $.__views.__alloyId60 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "Los términos de uso que a continuación se presentan (en lo sucesivo los TÉRMINOS DE USO) constituyen el acuerdo entre el Gobierno de Reynosa (en lo sucesivo el MUNICIPIO) y cualquier usuario de esta aplicación móvil (en lo sucesivo la APLICACIÓN).",
        id: "__alloyId60"
    });
    $.__views.container.add($.__views.__alloyId60);
    $.__views.__alloyId61 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "La utilización de la APLICACIÓN por parte de cualquier persona, le atribuye la calidad de usuario (en lo sucesivo el USUARIO) y ello implica su adhesión plena e incondicional a estos TÉRMINOS DE USO, en consecuencia es indispensable que el USUARIO los lea previamente y los evalúe de forma cuidadosa, de tal manera que esté consciente de que se sujeta a ellos y a las modificaciones que pudieran sufrir, cada vez que accede a la APLICACIÓN.",
        id: "__alloyId61"
    });
    $.__views.container.add($.__views.__alloyId61);
    $.__views.__alloyId62 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "Si en cualquier momento el USUARIO no estuviera de acuerdo total o parcialmente con estos TÉRMINOS DE USO, deberá abstenerse inmediatamente de usar la APLICACIÓN en cualquiera de sus partes o secciones.",
        id: "__alloyId62"
    });
    $.__views.container.add($.__views.__alloyId62);
    $.__views.__alloyId63 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "El USUARIO está de acuerdo en que en diversas partes de la APLICACIÓN podrá haber estipulaciones especiales que, según sea el caso, sustituirán, complementarán y/o modificarán los TÉRMINOS DE USO (en lo sucesivo las CONDICIONES PARTICULARES), por tal razón también resulta indispensable que el USUARIO las lea previamente en cada caso y de no estar total o parcialmente de acuerdo con ella o ellas, se abstenga de usar la parte o sección de la APLICACIÓN regulada bajo esa CONDICIÓN o CONDICIONES PARTICULAR(ES). En caso de que el USUARIO utilice la parte de la APLICACIÓN regulada por una o más CONDICIONES PARTICULARES, se entenderá que se somete total e incondicionalmente a ellas.",
        id: "__alloyId63"
    });
    $.__views.container.add($.__views.__alloyId63);
    $.__views.__alloyId64 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "OBJETO",
        id: "__alloyId64"
    });
    $.__views.container.add($.__views.__alloyId64);
    $.__views.__alloyId65 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: 'A través del SITIO, el MUNICIPIO facilita a los USUARIOS el acceso a información diversa proporcionada por el Gobierno Municipal  (en lo sucesivo los "CONTENIDOS").',
        id: "__alloyId65"
    });
    $.__views.container.add($.__views.__alloyId65);
    $.__views.__alloyId66 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "El USUARIO reconoce que el uso de la APLICACIÓN no le implica ningún derecho de propiedad sobre el mismo, cualquiera de sus elementos o CONTENIDOS.",
        id: "__alloyId66"
    });
    $.__views.container.add($.__views.__alloyId66);
    $.__views.__alloyId67 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "El MUNICIPIO se reserva el derecho a modificar en cualquier momento y sin aviso previo la presentación, configuración, información, CONTENIDOS y en general cualquier parte o aspecto relacionado directa o indirectamente con la APLICACIÓN.",
        id: "__alloyId67"
    });
    $.__views.container.add($.__views.__alloyId67);
    $.__views.__alloyId68 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "ACCESO Y UTILIZACIÓN DE LA APLICACIÓN",
        id: "__alloyId68"
    });
    $.__views.container.add($.__views.__alloyId68);
    $.__views.__alloyId69 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "El USUARIO estará obligado a registrarse en la APLICACIÓN, bajo las condiciones que se establezcan por parte del MUNICIPIO.",
        id: "__alloyId69"
    });
    $.__views.container.add($.__views.__alloyId69);
    $.__views.__alloyId70 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "OBLIGACIÓN DE HACER USO CORRECTO DE LA APLICACIÓN Y DE LOS CONTENIDOS.",
        id: "__alloyId70"
    });
    $.__views.container.add($.__views.__alloyId70);
    $.__views.__alloyId71 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "El USUARIO se compromete a utilizar la APLICACIÓN y los CONTENIDOS conforme a las leyes aplicables, a lo dispuesto en estos TÉRMINOS DE USO y con respeto al orden público. El USUARIO se obliga utilizar la APLICACIÓN y cualquier CONTENIDO o aspecto relacionado con él, de una manera en que no lesione derechos o intereses del MUNICIPIO, o de personas vinculadas a éste directa o indirectamente o de terceros. EL USUARIO utilizará la APLICACIÓN y/o los CONTENIDOS de una manera en la que no los dañe, inutilice, deteriore o menoscabe total o parcialmente.",
        id: "__alloyId71"
    });
    $.__views.container.add($.__views.__alloyId71);
    $.__views.__alloyId72 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "MEDIOS PARA OBTENCIÓN DE CONTENIDOS",
        id: "__alloyId72"
    });
    $.__views.container.add($.__views.__alloyId72);
    $.__views.__alloyId73 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "Se podrá hacer uso de los CONTENIDOS y los elementos que se encuentran en la APLICACIÓN empleando para ello únicamente los medios o procedimientos establecidos dentro del mismo.",
        id: "__alloyId73"
    });
    $.__views.container.add($.__views.__alloyId73);
    $.__views.__alloyId74 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "USO CORRECTO DE LOS CONTENIDOS",
        id: "__alloyId74"
    });
    $.__views.container.add($.__views.__alloyId74);
    $.__views.__alloyId75 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "El USUARIO se obliga a usar los CONTENIDOS y los elementos utilizados en la APLICACIÓN de forma diligente, correcta y lícita, y en particular, se compromete a abstenerse de: (a) utilizar los CONTENIDOS de forma, con fines o efectos contrarios a la ley, al orden público y a lo establecido por el MUNICIPIO para el uso de esta APLICACIÓN; (b) modificar y/o distribuir o utilizar de cualquier forma con o sin fines de lucro los CONTENIDOS y los elementos utilizados en la APLICACIÓN, a menos que se cuente con la autorización expresa y por escrito de MILENIO; (c) modificar o manipular la información, las marcas, logotipos y signos distintivos en general del MUNICIPIO, de la APLICACIÓN o de las personas vinculadas directa o indirectamente con el MUNICIPIO (salvo que cuente con su autorización por escrito), (d) suprimir, eludir o modificar los CONTENIDOS y los elementos utilizados en la APLICACIÓN, así como los dispositivos técnicos de protección, o cualquier mecanismo o procedimiento establecido en la APLICACIÓN.",
        id: "__alloyId75"
    });
    $.__views.container.add($.__views.__alloyId75);
    $.__views.__alloyId76 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "OBLIGACIÓN DE OBSERVAR LAS INSTRUCCIONES",
        id: "__alloyId76"
    });
    $.__views.container.add($.__views.__alloyId76);
    $.__views.__alloyId77 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "El USUARIO se compromete a seguir al pie de la letra cualquier instrucción impartida por el MUNICIPIO o por personal autorizado por el MUNICIPIO que se encuentre relacionado con la APLICACIÓN, en relación con cualquier mecanismo o procedimiento establecido para usar la APLICACIÓN y los CONTENIDOS. El USUARIO únicamente deberá seguir instrucciones impartidas por el MUNICIPIO.",
        id: "__alloyId77"
    });
    $.__views.container.add($.__views.__alloyId77);
    $.__views.__alloyId78 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "DATOS DE CARÁCTER PERSONAL",
        id: "__alloyId78"
    });
    $.__views.container.add($.__views.__alloyId78);
    $.__views.__alloyId79 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "Para utilizar o gozar de algunos de los CONTENIDOS es necesario que el USUARIO proporcione previamente al MUNICIPIO ciertos datos de carácter personal (en adelante DATOS PERSONALES) que el MUNICIPIO podrá administrar automatizadamente o no. El USUARIO al acceder a la APLICACIÓN o a cualquiera de los CONTENIDOS en que los DATOS PERSONALES son requeridos, está autorizando al MUNICIPIO a realizar análisis y estudios con base en ellos. El USUARIO se obliga a proporcionar DATOS PERSONALES verdaderos y fidedignos, y en caso de que deseara no darlos en las partes de la APLICACIÓN que se soliciten, deberá abstenerse de usar esa o esas partes o secciones de la APLICACIÓN. En caso de que el USUARIO diera información falsa o confusa, el MUNICIPIO podrá negarle el acceso a la APLICACIÓN y los CONTENIDOS dentro de él, sin perjuicio de que pueda requerirle las indemnizaciones a que hubiere lugar. Por el hecho de proporcionar sus DATOS PERSONALES en la APLICACIÓN, el USUARIO está autorizando al MUNICIPIO a dar a conocer a cualquier autoridad competente la información respectiva, en caso de que ésta sea solicitada por los medios jurídicos adecuados. El MUNICIPIO no compartirá información personal alguna que haya sido proporcionada, con terceras personas, a menos que sea requerido para proporcionar un servicio o un producto requerido por el USUARIO.",
        id: "__alloyId79"
    });
    $.__views.container.add($.__views.__alloyId79);
    $.__views.__alloyId80 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "UTILIZACIÓN DEL SITIO Y DE LOS CONTENIDOS BAJO LA EXCLUSIVA RESPONSABILIDAD DEL USUARIO",
        id: "__alloyId80"
    });
    $.__views.container.add($.__views.__alloyId80);
    $.__views.__alloyId81 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "Por el sólo hecho de acceder a la APLICACIÓN, el USUARIO reconoce y acepta que el uso del mismo y de los CONTENIDOS, es bajo su exclusiva y estricta responsabilidad, por lo que el MUNICIPIO no será en ningún momento y bajo ninguna circunstancia, responsable por cualquier desperfecto o problema que se presentara en el equipo de cómputo (hardware) o programas de cómputo (software) que utilice el USUARIO para acceder o navegar en cualquier parte de la APLICACIÓN.",
        id: "__alloyId81"
    });
    $.__views.container.add($.__views.__alloyId81);
    $.__views.__alloyId82 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "EXCLUSIÓN DE GARANTÍAS Y DE RESPONSABILIDAD",
        id: "__alloyId82"
    });
    $.__views.container.add($.__views.__alloyId82);
    $.__views.__alloyId83 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "Exclusión de Garantías y de Responsabilidad por el Funcionamiento de la APLICACIÓN y de los CONTENIDOS, virus y otros elementos dañinos: el MUNICIPIO no controla y no garantiza la ausencia de virus en los CONTENIDOS, ni la ausencia de otros elementos en los CONTENIDOS que puedan producir alteraciones en el sistema informático del USUARIO (software y/o hardware) o en los documentos electrónicos y ficheros almacenados en su sistema informático.",
        id: "__alloyId83"
    });
    $.__views.container.add($.__views.__alloyId83);
    $.__views.__alloyId84 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "LICENCIA",
        id: "__alloyId84"
    });
    $.__views.container.add($.__views.__alloyId84);
    $.__views.__alloyId85 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "El MUNICIPIO autoriza al USUARIO a utilizar la APLICACIÓN exclusivamente en los TÉRMINOS Y CONDICIONES aquí expresados, sin que ello implique que concede al USUARIO licencia o autorización alguna o algún tipo de derecho distinto al uso antes mencionado.",
        id: "__alloyId85"
    });
    $.__views.container.add($.__views.__alloyId85);
    $.__views.__alloyId86 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "NEGACIÓN Y RETIRO DEL ACCESO AL SITIO Y/O A LOS CONTENIDOS",
        id: "__alloyId86"
    });
    $.__views.container.add($.__views.__alloyId86);
    $.__views.__alloyId87 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "El MUNICIPIO se reserva el derecho a negar o retirar el acceso a la APLICACIÓN y/o a los CONTENIDOS en cualquier momento y sin previo aviso, por iniciativa propia o a petición de cualquier persona, a cualquier USUARIO por cualquier motivo, incluyendo sin limitación a aquellos USUARIOS que den un uso indebido a la APLICACIÓN, a cualquiera de sus partes o secciones o a los CONTENIDOS, o que incumplan total o parcialmente estos TERMINOS DE USO o las CONDICIONES PARTICULARES que resulten aplicables.",
        id: "__alloyId87"
    });
    $.__views.container.add($.__views.__alloyId87);
    $.__views.__alloyId88 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "DURACIÓN Y TERMINACIÓN",
        id: "__alloyId88"
    });
    $.__views.container.add($.__views.__alloyId88);
    $.__views.__alloyId89 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "La APLICACIÓN y los CONTENIDOS tienen una duración indefinida. Sin embargo, el MUNICIPIO podrá dar por terminada, suspender o interrumpir en cualquier momento y sin necesidad de previo aviso la APLICACIÓN y/o cualquiera de los CONTENIDOS.",
        id: "__alloyId89"
    });
    $.__views.container.add($.__views.__alloyId89);
    $.__views.__alloyId90 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "OTRAS DISPOSICIONES",
        id: "__alloyId90"
    });
    $.__views.container.add($.__views.__alloyId90);
    $.__views.__alloyId91 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "*Forma. El USUARIO acepta que una versión impresa de estos TERMINOS Y CONDICIONES, y de cualquier comunicación enviada y/o recibida en forma electrónica será admisible como medio probatorio en cualquier procedimiento judicial y/o administrativo. *Ley aplicable y jurisdicción. En todo lo relacionado con la interpretación y cumplimiento de lo aquí dispuesto, por el solo hecho de acceder a la APLICACIÓN los USUARIOS aceptan someterse a las leyes aplicables y a la jurisdicción de los tribunales competentes en la ciudad de Reynosa, Tamaulipas., México, renunciado a cualquier otra jurisdicción que por razón de sus domicilios presentes o futuros, o por cualquiera otra razón pudiese corresponderles.",
        id: "__alloyId91"
    });
    $.__views.container.add($.__views.__alloyId91);
    $.__views.__alloyId92 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "DISPOSICIONES INEFICACES",
        id: "__alloyId92"
    });
    $.__views.container.add($.__views.__alloyId92);
    $.__views.__alloyId93 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "En caso de que alguno de los TÉRMINOS Y CONDICIONES aquí expresados sea considerado no ejecutable o se declare nulo o inválido de acuerdo con la legislación aplicable, éste será sustituido por un término o condición que sea válido y que pueda cumplir con mayor rigor el objetivo de la disposición no ejecutable o que haya sido declarada nula o inválida. Los demás términos y condiciones continuarán en plena vigencia.",
        id: "__alloyId93"
    });
    $.__views.container.add($.__views.__alloyId93);
    $.__views.__alloyId94 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontWeight: "bold",
            fontSize: "16dp"
        },
        left: 0,
        width: Ti.UI.FILL,
        top: "10dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        text: "DERECHOS",
        id: "__alloyId94"
    });
    $.__views.container.add($.__views.__alloyId94);
    $.__views.__alloyId95 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "Cualquier derecho que no se haya conferido expresamente en este documento se entiende reservado al MUNICIPIO.",
        id: "__alloyId95"
    });
    $.__views.container.add($.__views.__alloyId95);
    $.__views.__alloyId96 = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontSize: "14dp"
        },
        left: 0,
        top: "10dp",
        text: "",
        id: "__alloyId96"
    });
    $.__views.container.add($.__views.__alloyId96);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var CONFIG = arguments[0];
    $.init = function() {
        (CONFIG.hasOwnProperty("menu") ? CONFIG.back : true) && $.NavigationBar.showMenu(function(_event) {
            _event.cancelBubble = true;
            $.NavigationBar.toggleMenu();
        });
        (CONFIG.hasOwnProperty("back") ? CONFIG.back : true) && $.NavigationBar.showBack(function(_event) {
            _event.cancelBubble = true;
            APP.removeChild();
        });
    };
    $.init();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;