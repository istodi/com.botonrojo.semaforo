function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "list";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.Wrapper = Ti.UI.createView({
        id: "Wrapper",
        name: "Alerts list"
    });
    $.__views.Wrapper && $.addTopLevelView($.__views.Wrapper);
    $.__views.NavigationBar = Alloy.createWidget("com.mcongrove.navigationBar", "widget", {
        id: "NavigationBar",
        text: "SEMÁFORO REYNOSA",
        __parentSymbol: $.__views.Wrapper
    });
    $.__views.NavigationBar.setParent($.__views.Wrapper);
    $.__views.container = Ti.UI.createTableView({
        top: "47dp",
        backgroundColor: "#FFF",
        layout: "vertical",
        showVerticalScrollIndicator: true,
        separatorColor: "#cccccc",
        minRowHeight: "80dp",
        id: "container"
    });
    $.__views.Wrapper.add($.__views.container);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var MODEL = require("models/alert");
    arguments[0];
    var rows = [];
    $.init = function() {
        APP.log("error", "alerts.init");
        $.NavigationBar.showMenu(function(_event) {
            _event.cancelBubble = true;
            $.NavigationBar.toggleMenu();
        });
        data = MODEL.getAll();
        $.handleData(data);
    };
    Ti.App.addEventListener("alerts.fetched", function() {
        APP.log("error", "actualizar lista de rutas");
        $.handleData(MODEL.getAll());
    });
    $.handleData = function(data) {
        APP.log("error", "routes.handleData");
        rows = [];
        _(data).each(function(_datum) {
            var row = Alloy.createController("row", {
                id: _datum.id,
                name: _datum.name,
                alert_type: _datum.alert_type,
                created: _datum.created
            }).getView();
            _datum.isChild = true;
            row.rowData = _datum;
            rows.push(row);
        });
        $.container.setData(rows);
    };
    $.init();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;