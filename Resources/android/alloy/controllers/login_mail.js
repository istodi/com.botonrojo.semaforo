function Controller() {
    function login() {
        var valid = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test($.mail.value);
        if (valid) if ($.password.value === $.passwordConfirmation.value) {
            var data = {
                mail: $.mail.value,
                pass: Ti.Utils.sha256($.password.value),
                name: "",
                login_type: "3",
                twitter_username: ""
            };
            SYNC.login(data);
        } else alert("Las contraseñas son diferentes"); else alert("Correo electrónico invalido");
    }
    function showTerms() {
        APP.addChild("terms", {
            menu: false
        }, true);
    }
    function switchOrientation(orientation) {
        if (2 === orientation) {
            $.logo.applyProperties({
                top: "22%",
                left: "2%",
                width: "40%",
                height: Ti.UI.SIZE
            });
            $.buttonContainer.applyProperties({
                top: "5%",
                left: "45%",
                right: "2%",
                width: Ti.UI.FILL
            });
        } else if (1 === orientation) {
            $.logo.applyProperties({
                top: "7%",
                left: "auto",
                height: "21%",
                width: Ti.UI.SIZE
            });
            $.buttonContainer.applyProperties({
                top: "30%",
                left: "12.5%",
                right: "12.5%",
                width: "75%"
            });
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "login_mail";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.loginView = Ti.UI.createView({
        width: Ti.UI.FILL,
        height: Ti.UI.FILL,
        id: "loginView"
    });
    $.__views.loginView && $.addTopLevelView($.__views.loginView);
    $.__views.logo = Ti.UI.createImageView({
        height: "21%",
        top: "7%",
        center: {
            x: "50%",
            y: "50%"
        },
        image: "/images/logo.png",
        touchEnabled: false,
        id: "logo"
    });
    $.__views.loginView.add($.__views.logo);
    $.__views.buttonContainer = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.FILL,
        top: "30%",
        bottom: "14%",
        width: "75%",
        id: "buttonContainer"
    });
    $.__views.loginView.add($.__views.buttonContainer);
    $.__views.__alloyId30 = Ti.UI.createLabel({
        color: "#000",
        text: "Ingresa tus datos:",
        bottom: "10dp",
        id: "__alloyId30"
    });
    $.__views.buttonContainer.add($.__views.__alloyId30);
    $.__views.mail = Ti.UI.createTextField({
        color: "#000",
        backgroundColor: "#FFF",
        borderWidth: 1,
        borderColor: "#bbb",
        width: "100%",
        bottom: "10dp",
        font: {
            fontSize: "14dp",
            fontFamily: "HelveticaNeue-Light"
        },
        id: "mail",
        autocorrect: "false",
        hintText: "Correo electrónico"
    });
    $.__views.buttonContainer.add($.__views.mail);
    $.__views.password = Ti.UI.createTextField({
        color: "#000",
        backgroundColor: "#FFF",
        borderWidth: 1,
        borderColor: "#bbb",
        width: "100%",
        bottom: "10dp",
        font: {
            fontSize: "14dp",
            fontFamily: "HelveticaNeue-Light"
        },
        id: "password",
        hintText: "Contraseña",
        passwordMask: "true"
    });
    $.__views.buttonContainer.add($.__views.password);
    $.__views.passwordConfirmation = Ti.UI.createTextField({
        color: "#000",
        backgroundColor: "#FFF",
        borderWidth: 1,
        borderColor: "#bbb",
        width: "100%",
        bottom: "10dp",
        font: {
            fontSize: "14dp",
            fontFamily: "HelveticaNeue-Light"
        },
        id: "passwordConfirmation",
        hintText: "Confirmar contraseña",
        passwordMask: "true"
    });
    $.__views.buttonContainer.add($.__views.passwordConfirmation);
    $.__views.save = Ti.UI.createView({
        id: "save",
        backgroundColor: "#C0272A",
        bottom: "10dp",
        width: "50%",
        height: Ti.UI.SIZE
    });
    $.__views.buttonContainer.add($.__views.save);
    login ? $.__views.save.addEventListener("click", login) : __defers["$.__views.save!click!login"] = true;
    $.__views.__alloyId31 = Ti.UI.createLabel({
        color: "#FFF",
        text: "INGRESAR",
        top: "5dp",
        bottom: "5dp",
        id: "__alloyId31"
    });
    $.__views.save.add($.__views.__alloyId31);
    $.__views.__alloyId32 = Ti.UI.createLabel({
        color: "#000",
        text: "Leer términos y condiciones",
        width: "100%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        id: "__alloyId32"
    });
    $.__views.buttonContainer.add($.__views.__alloyId32);
    showTerms ? $.__views.__alloyId32.addEventListener("click", showTerms) : __defers["$.__views.__alloyId32!click!showTerms"] = true;
    $.__views.footer = Ti.UI.createView({
        height: "14%",
        width: Ti.UI.FILL,
        bottom: "1dp",
        left: "5dp",
        right: "5dp",
        id: "footer"
    });
    $.__views.loginView.add($.__views.footer);
    $.__views.logoReynosa = Ti.UI.createImageView({
        left: "10dp",
        right: "60%",
        height: Ti.UI.FILL,
        width: Ti.UI.SIZE,
        center: {
            x: "50%",
            y: "50%"
        },
        image: "/images/logoReynosa.png",
        id: "logoReynosa"
    });
    $.__views.footer.add($.__views.logoReynosa);
    $.__views.developedBy = Ti.UI.createLabel({
        color: "#000",
        width: "40%",
        right: "5dp",
        id: "developedBy",
        text: "Desarrollo por Botón Rojo",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
    });
    $.__views.footer.add($.__views.developedBy);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var SYNC = require("sync");
    $.init = function() {
        Ti.API.error("init");
        $.loginView.backgroundImage = "/images/bg.png";
        $.mail.KeyboardType = Ti.UI.KEYBOARD_EMAIL;
    };
    Ti.Gesture.addEventListener("orientationchange", function() {
        switchOrientation(Titanium.Gesture.orientation);
    });
    switchOrientation(Titanium.Gesture.orientation);
    $.init();
    __defers["$.__views.save!click!login"] && $.__views.save.addEventListener("click", login);
    __defers["$.__views.__alloyId32!click!showTerms"] && $.__views.__alloyId32.addEventListener("click", showTerms);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;