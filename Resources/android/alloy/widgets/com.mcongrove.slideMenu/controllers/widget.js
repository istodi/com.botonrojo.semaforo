function WPATH(s) {
    var index = s.lastIndexOf("/");
    var path = -1 === index ? "com.mcongrove.slideMenu/" + s : s.substring(0, index) + "/com.mcongrove.slideMenu/" + s.substring(index + 1);
    return true && 0 !== path.indexOf("/") ? "/" + path : path;
}

function Controller() {
    function handleClick(_event) {
        "undefined" != typeof _event.index && $.setIndex(_event.index);
    }
    new (require("alloy/widget"))("com.mcongrove.slideMenu");
    this.__widgetId = "com.mcongrove.slideMenu";
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "widget";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.Wrapper = Ti.UI.createView({
        width: "150dp",
        height: Ti.UI.SIZE,
        top: "47dp",
        right: 0,
        id: "Wrapper"
    });
    $.__views.Wrapper && $.addTopLevelView($.__views.Wrapper);
    $.__views.Nodes = Ti.UI.createTableView({
        top: "0dp",
        backgroundColor: "#000",
        separatorColor: "#FFF",
        id: "Nodes"
    });
    $.__views.Wrapper.add($.__views.Nodes);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var sections = [];
    var nodes = [];
    var color;
    $.init = function(_params) {
        sections = [];
        nodes = [];
        color = "undefined" != typeof _params.color ? _params.color : null;
        for (var i = 0; _params.nodes.length > i; i++) {
            var tab = Ti.UI.createTableViewRow({
                id: _params.nodes[i].id,
                height: "40dp",
                backgroundSelectedColor: "#ABC",
                selectedBackgroundColor: "#DCA"
            });
            var label = Ti.UI.createLabel({
                text: _params.nodes[i].title,
                top: "0dp",
                left: "10dp",
                right: "10dp",
                height: "39dp",
                font: {
                    fontSize: "14dp",
                    fontFamily: "HelveticaNeue-Light"
                },
                color: "#FFF",
                touchEnabled: false
            });
            tab.add(label);
            nodes.push(tab);
        }
        nodes.length > 0 && $.Nodes.setData(nodes);
        $.Nodes.removeEventListener("click", handleClick);
        $.Nodes.addEventListener("click", handleClick);
    };
    $.clear = function() {
        $.Nodes.setData([]);
        $.Nodes.removeAllChildren();
    };
    $.setIndex = function(_index) {
        $.Nodes.selectRow(_index);
    };
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;