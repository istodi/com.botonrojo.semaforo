function WPATH(s) {
    var index = s.lastIndexOf("/");
    var path = -1 === index ? "com.mcongrove.slideMenu/" + s : s.substring(0, index) + "/com.mcongrove.slideMenu/" + s.substring(index + 1);
    return true && 0 !== path.indexOf("/") ? "/" + path : path;
}

function Controller() {
    new (require("alloy/widget"))("com.mcongrove.slideMenu");
    this.__widgetId = "com.mcongrove.slideMenu";
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "config";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.Wrapper = Ti.UI.createView({
        layout: "vertical",
        id: "Wrapper",
        name: "Settings"
    });
    $.__views.Wrapper && $.addTopLevelView($.__views.Wrapper);
    $.__views.NavigationBar = Alloy.createWidget("com.mcongrove.navigationBar", "widget", {
        id: "NavigationBar",
        text: "SEMÁFORO REYNOSA",
        __parentSymbol: $.__views.Wrapper
    });
    $.__views.NavigationBar.setParent($.__views.Wrapper);
    $.__views.header = Ti.UI.createView({
        id: "header"
    });
    $.__views.Wrapper.add($.__views.header);
    $.__views.__alloyId0 = Ti.UI.createLabel({
        text: "Elige las alertas que deseas recibir:",
        id: "__alloyId0"
    });
    $.__views.header.add($.__views.__alloyId0);
    $.__views.__alloyId1 = Ti.UI.createLabel({
        text: "Notificaciones",
        id: "__alloyId1"
    });
    $.__views.Wrapper.add($.__views.__alloyId1);
    $.__views.__alloyId2 = Ti.UI.createView({
        id: "__alloyId2"
    });
    $.__views.Wrapper.add($.__views.__alloyId2);
    $.__views.alert - 0 = Ti.UI.createLabel({
        id: "alert-0",
        text: "Verde"
    });
    $.__views.__alloyId2.add($.__views.alert - 0);
    $.__views.__alloyId3 = Ti.UI.createView({
        id: "__alloyId3"
    });
    $.__views.Wrapper.add($.__views.__alloyId3);
    $.__views.alert - 1 = Ti.UI.createLabel({
        id: "alert-1",
        text: "Amarillo"
    });
    $.__views.__alloyId3.add($.__views.alert - 1);
    $.__views.__alloyId4 = Ti.UI.createView({
        id: "__alloyId4"
    });
    $.__views.Wrapper.add($.__views.__alloyId4);
    $.__views.alert - 2 = Ti.UI.createLabel({
        id: "alert-2",
        text: "Naranja"
    });
    $.__views.__alloyId4.add($.__views.alert - 2);
    $.__views.__alloyId5 = Ti.UI.createView({
        id: "__alloyId5"
    });
    $.__views.Wrapper.add($.__views.__alloyId5);
    $.__views.alert - 3 = Ti.UI.createLabel({
        id: "alert-3",
        text: "Rojo"
    });
    $.__views.__alloyId5.add($.__views.alert - 3);
    $.__views.__alloyId6 = Ti.UI.createLabel({
        text: "Zonas",
        id: "__alloyId6"
    });
    $.__views.Wrapper.add($.__views.__alloyId6);
    $.__views.__alloyId7 = Ti.UI.createView({
        id: "__alloyId7"
    });
    $.__views.Wrapper.add($.__views.__alloyId7);
    $.__views.zone - 1 = Ti.UI.createLabel({
        id: "zone-1",
        text: "Todas"
    });
    $.__views.__alloyId7.add($.__views.zone - 1);
    $.__views.__alloyId8 = Ti.UI.createView({
        id: "__alloyId8"
    });
    $.__views.Wrapper.add($.__views.__alloyId8);
    $.__views.zone - 0 = Ti.UI.createLabel({
        id: "zone-0",
        text: "Cerca de mi ubicación"
    });
    $.__views.__alloyId8.add($.__views.zone - 0);
    $.__views.__alloyId9 = Ti.UI.createView({
        id: "__alloyId9"
    });
    $.__views.Wrapper.add($.__views.__alloyId9);
    $.__views.zone - 0 = Ti.UI.createLabel({
        id: "zone-0",
        text: "GUARDAR CAMBIOS"
    });
    $.__views.__alloyId9.add($.__views.zone - 0);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;