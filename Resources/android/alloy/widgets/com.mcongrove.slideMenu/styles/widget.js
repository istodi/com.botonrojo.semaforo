function WPATH(s) {
    var index = s.lastIndexOf("/");
    var path = -1 === index ? "com.mcongrove.slideMenu/" + s : s.substring(0, index) + "/com.mcongrove.slideMenu/" + s.substring(index + 1);
    return true && 0 !== path.indexOf("/") ? "/" + path : path;
}

module.exports = [ {
    isApi: true,
    priority: 1000.0001,
    key: "TextField",
    style: {
        color: "#000",
        backgroundColor: "#FFF",
        borderWidth: 1,
        borderColor: "#bbb"
    }
}, {
    isId: true,
    priority: 100000.0015,
    key: "Wrapper",
    style: {
        width: "150dp",
        height: Ti.UI.SIZE,
        top: "47dp",
        right: 0
    }
}, {
    isId: true,
    priority: 100000.0016,
    key: "Nodes",
    style: {
        top: "0dp",
        backgroundColor: "#000",
        separatorColor: "#FFF"
    }
} ];