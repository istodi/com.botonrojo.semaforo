function WPATH(s) {
    var index = s.lastIndexOf("/");
    var path = -1 === index ? "com.kliento.loading/" + s : s.substring(0, index) + "/com.kliento.loading/" + s.substring(index + 1);
    return true && 0 !== path.indexOf("/") ? "/" + path : path;
}

module.exports = [ {
    isApi: true,
    priority: 1000.0001,
    key: "TextField",
    style: {
        color: "#000",
        backgroundColor: "#FFF",
        borderWidth: 1,
        borderColor: "#bbb"
    }
}, {
    isId: true,
    priority: 100000.0015,
    key: "Wrapper",
    style: {
        width: Ti.UI.FILL,
        top: "0dp"
    }
}, {
    isId: true,
    priority: 100000.0016,
    key: "Background",
    style: {
        backgroundColor: "#000",
        opacity: .3,
        width: Ti.UI.FILL,
        height: Ti.UI.FILL
    }
}, {
    isId: true,
    priority: 100000.0017,
    key: "Modal",
    style: {
        width: Ti.UI.SIZE,
        height: "92dp",
        borderRadius: 10,
        backgroundColor: "#000",
        layout: "vertical"
    }
}, {
    isId: true,
    priority: 100000.0018,
    key: "Loading",
    style: {
        height: "33dp",
        width: "33dp",
        top: "17dp",
        backgroundColor: "#000",
        images: [ WPATH("images/00.png"), WPATH("images/01.png"), WPATH("images/02.png"), WPATH("images/03.png"), WPATH("images/04.png"), WPATH("images/05.png"), WPATH("images/06.png"), WPATH("images/07.png"), WPATH("images/08.png"), WPATH("images/09.png"), WPATH("images/10.png"), WPATH("images/11.png") ]
    }
}, {
    isId: true,
    priority: 100000.0019,
    key: "Label",
    style: {
        left: 5,
        right: 5,
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#FFF",
        textAlign: "center",
        font: {
            fontSize: "18dp",
            fontWeight: "bold"
        }
    }
} ];