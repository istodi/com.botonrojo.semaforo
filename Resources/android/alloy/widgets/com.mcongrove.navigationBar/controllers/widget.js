function WPATH(s) {
    var index = s.lastIndexOf("/");
    var path = -1 === index ? "com.mcongrove.navigationBar/" + s : s.substring(0, index) + "/com.mcongrove.navigationBar/" + s.substring(index + 1);
    return true && 0 !== path.indexOf("/") ? "/" + path : path;
}

function Controller() {
    function config() {
        Ti.API.error(APP.controllerStacks);
        APP.controllerStacks.length > 1 && APP.removeChild();
        APP.addChild("config", {}, true);
    }
    function help() {
        Ti.API.error(APP.controllerStacks);
        APP.controllerStacks.length > 1 && APP.removeChild();
        APP.addChild("help", {}, true);
    }
    function terms() {
        Ti.API.error(APP.controllerStacks);
        APP.controllerStacks.length > 1 && APP.removeChild();
        APP.addChild("terms", {}, true);
    }
    function close() {
        Ti.App.Properties.setBool("loggedIn", false);
        Ti.App.Properties.setInt("userId", 0);
        Ti.App.Properties.setBool("alert_1", true);
        Ti.App.Properties.setBool("alert_2", true);
        Ti.App.Properties.setBool("alert_3", true);
        Ti.App.Properties.setBool("alert_4", true);
        require("net.iamyellow.gcmjs").unregisterForPushNotifications();
        fb.loggedIn && fb.logout();
        APP.addChild("login", {}, false);
    }
    function hexToHsb(_hex) {
        var result;
        result = 6 > _hex.length ? /^#?([a-f\d]{1})([a-f\d]{1})([a-f\d]{1})$/i.exec(_hex) : /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(_hex);
        var hsb = {
            h: 0,
            s: 0,
            b: 0
        };
        if (!result) return hsb;
        if (1 == result[1].length) {
            result[1] = result[1] + result[1];
            result[2] = result[2] + result[2];
            result[3] = result[3] + result[3];
        }
        var rgb = {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        };
        rgb.r /= 255;
        rgb.g /= 255;
        rgb.b /= 255;
        var del_r, del_g, del_b, minVal = Math.min(rgb.r, rgb.g, rgb.b), maxVal = Math.max(rgb.r, rgb.g, rgb.b), delta = maxVal - minVal;
        hsb.b = maxVal;
        if (0 !== delta) {
            hsb.s = delta / maxVal;
            del_r = ((maxVal - rgb.r) / 6 + delta / 2) / delta;
            del_g = ((maxVal - rgb.g) / 6 + delta / 2) / delta;
            del_b = ((maxVal - rgb.b) / 6 + delta / 2) / delta;
            rgb.r === maxVal ? hsb.h = del_b - del_g : rgb.g === maxVal ? hsb.h = 1 / 3 + del_r - del_b : rgb.b === maxVal && (hsb.h = 2 / 3 + del_g - del_r);
            0 > hsb.h && (hsb.h += 1);
            hsb.h > 1 && (hsb.h -= 1);
        }
        hsb.h = Math.round(360 * hsb.h);
        hsb.s = Math.round(100 * hsb.s);
        hsb.b = Math.round(100 * hsb.b);
        return hsb;
    }
    new (require("alloy/widget"))("com.mcongrove.navigationBar");
    this.__widgetId = "com.mcongrove.navigationBar";
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "widget";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.superWrapper = Ti.UI.createView({
        top: "0dp",
        right: "0dp",
        height: "47dp",
        width: Ti.UI.FILL,
        id: "superWrapper",
        zIndex: "1"
    });
    $.__views.superWrapper && $.addTopLevelView($.__views.superWrapper);
    $.__views.Wrapper = Ti.UI.createView({
        top: "0dp",
        left: "0dp",
        width: Ti.UI.FILL,
        height: "47dp",
        backgroundColor: "#e6e6e6",
        id: "Wrapper"
    });
    $.__views.superWrapper.add($.__views.Wrapper);
    $.__views.overlay = Ti.UI.createView({
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        left: "0dp",
        top: "0dp",
        id: "overlay"
    });
    $.__views.Wrapper.add($.__views.overlay);
    $.__views.back = Ti.UI.createView({
        top: "0dp",
        left: "0dp",
        width: "48dp",
        height: "47dp",
        id: "back",
        visible: "false"
    });
    $.__views.overlay.add($.__views.back);
    $.__views.backImage = Ti.UI.createImageView({
        top: "13dp",
        left: "9dp",
        width: "32dp",
        height: "21dp",
        preventDefaultImage: true,
        id: "backImage"
    });
    $.__views.back.add($.__views.backImage);
    $.__views.next = Ti.UI.createView({
        top: "0dp",
        right: "0dp",
        width: "48dp",
        height: "47dp",
        id: "next",
        visible: "false"
    });
    $.__views.overlay.add($.__views.next);
    $.__views.nextImage = Ti.UI.createImageView({
        top: "9dp",
        right: "9dp",
        width: "28dp",
        height: "28dp",
        preventDefaultImage: true,
        id: "nextImage"
    });
    $.__views.next.add($.__views.nextImage);
    $.__views.left = Ti.UI.createView({
        top: "0dp",
        left: "0dp",
        width: "48dp",
        height: "47dp",
        id: "left",
        visible: "false"
    });
    $.__views.overlay.add($.__views.left);
    $.__views.leftImage = Ti.UI.createImageView({
        top: "13dp",
        left: "9dp",
        width: "32dp",
        height: "21dp",
        id: "leftImage"
    });
    $.__views.left.add($.__views.leftImage);
    $.__views.border = Ti.UI.createView({
        bottom: "0dp",
        height: "1dp",
        width: Ti.UI.FILL,
        backgroundColor: "#999999",
        opacity: .2,
        id: "border"
    });
    $.__views.Wrapper.add($.__views.border);
    $.__views.right = Ti.UI.createView({
        top: "0dp",
        right: "0dp",
        width: "48dp",
        height: "47dp",
        zIndex: 1,
        id: "right"
    });
    $.__views.superWrapper.add($.__views.right);
    $.__views.rightImage = Ti.UI.createImageView({
        top: "13dp",
        right: "9dp",
        width: "32dp",
        height: "21dp",
        borderRadius: 4,
        id: "rightImage"
    });
    $.__views.right.add($.__views.rightImage);
    var __alloyId0 = [];
    $.__views.__alloyId1 = Ti.UI.createTableViewRow({
        title: "Configurar alertas",
        color: "#FFF",
        id: "__alloyId1"
    });
    __alloyId0.push($.__views.__alloyId1);
    config ? $.__views.__alloyId1.addEventListener("click", config) : __defers["$.__views.__alloyId1!click!config"] = true;
    $.__views.__alloyId2 = Ti.UI.createTableViewRow({
        title: "Tipos de alertas",
        color: "#FFF",
        id: "__alloyId2"
    });
    __alloyId0.push($.__views.__alloyId2);
    help ? $.__views.__alloyId2.addEventListener("click", help) : __defers["$.__views.__alloyId2!click!help"] = true;
    $.__views.__alloyId3 = Ti.UI.createTableViewRow({
        title: "Términos y Condiciones",
        color: "#FFF",
        id: "__alloyId3"
    });
    __alloyId0.push($.__views.__alloyId3);
    terms ? $.__views.__alloyId3.addEventListener("click", terms) : __defers["$.__views.__alloyId3!click!terms"] = true;
    $.__views.__alloyId4 = Ti.UI.createTableViewRow({
        title: "Cerrar sesión",
        color: "#FFF",
        id: "__alloyId4"
    });
    __alloyId0.push($.__views.__alloyId4);
    close ? $.__views.__alloyId4.addEventListener("click", close) : __defers["$.__views.__alloyId4!click!close"] = true;
    $.__views.Nodes = Ti.UI.createTableView({
        width: "150dp",
        height: Ti.UI.SIZE,
        top: "47dp",
        right: 0,
        backgroundColor: "#000",
        separatorColor: "#FFF",
        data: __alloyId0,
        id: "Nodes"
    });
    $.__views.superWrapper.add($.__views.Nodes);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var CONFIG = arguments[0] || {};
    var navigation, theme;
    parseInt(Titanium.Platform.version.split(".")[0], 10);
    if (CONFIG.image) {
        var image = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, CONFIG.image);
        if (image.exists()) {
            image = image.nativePath;
            $.title = Ti.UI.createImageView({
                image: image,
                height: "26dp",
                width: Ti.UI.SIZE,
                top: "10dp",
                bottom: "10dp",
                preventDefaultImage: true
            });
        }
    } else $.title = Ti.UI.createLabel({
        top: "0dp",
        left: "58dp",
        right: "58dp",
        height: "46dp",
        font: {
            fontSize: "18dp",
            fontFamily: "HelveticaNeue-Medium"
        },
        color: "white" == theme ? "#FFF" : "#000",
        textAlign: "center",
        text: CONFIG.text ? CONFIG.text : ""
    });
    $.addNavigation = function(_view) {
        navigation = _view;
        $.Wrapper.add(navigation);
    };
    $.removeNavigation = function() {
        $.Wrapper.remove(navigation);
    };
    $.setBackgroundColor = function(_color) {
        $.Wrapper.backgroundColor = _color;
        theme = 65 > hexToHsb(_color).b ? "white" : "black";
    };
    $.setTitle = function(_text) {
        $.title.text = _text;
    };
    $.showLeft = function(_params) {
        if (_params && "undefined" != typeof _params.callback) {
            $.left.visible = true;
            $.leftImage.image = _params.image;
            $.left.addEventListener("click", _params.callback);
        }
    };
    $.showRight = function(_params) {
        if (_params && "undefined" != typeof _params.callback) {
            $.right.visible = true;
            $.rightImage.image = _params.image;
            $.right.addEventListener("click", _params.callback);
        }
    };
    $.showBack = function(_callback) {
        if (_callback && "undefined" != typeof _callback) {
            $.backImage.image = "/icons/black/back.png";
            $.back.visible = true;
            $.back.addEventListener("click", _callback);
        }
    };
    $.showNext = function(_callback) {
        if (_callback && "undefined" != typeof _callback) {
            $.nextImage.image = "white" == theme ? WPATH("/images/white/next.png") : WPATH("/images/black/next.png");
            $.next.visible = true;
            $.next.addEventListener("click", _callback);
        }
    };
    $.showMenu = function(_callback) {
        _callback && "undefined" != typeof _callback && $.showRight({
            image: "/icons/black/menu.png",
            callback: _callback
        });
    };
    $.showSettings = function(_callback) {
        _callback && "undefined" != typeof _callback && $.showRight({
            image: "/icons/black/refresh.png",
            callback: _callback
        });
    };
    $.rotateSettings = function(_callback) {
        var matrix2d = Ti.UI.create2DMatrix();
        matrix2d = matrix2d.rotate(180);
        var a = Ti.UI.createAnimation({
            transform: matrix2d,
            duration: 1e3
        });
        $.rightImage.animate(a);
        _callback && "undefined" != typeof _callback && _callback();
    };
    $.stopRotateSettings = function(_callback) {
        Titanium.UI.createAnimation();
        $.rightImage.animate();
        _callback && "undefined" != typeof _callback && _callback();
    };
    $.showAction = function(_callback) {
        _callback && "undefined" != typeof _callback && $.showRight({
            image: "white" == theme ? WPATH("/images/white/action.png") : WPATH("/images/black/action.png"),
            callback: _callback
        });
    };
    $.hideWrapper = function() {
        $.Wrapper.visible = false;
        $.Wrapper.height = 0;
        $.superWrapper.width = "49dp";
    };
    $.toggleMenu = function() {
        Ti.API.error("toggle menu");
        Ti.API.error($.superWrapper.getHeight());
        if ("47dp" === $.superWrapper.getHeight()) {
            Ti.API.error("show menu");
            $.superWrapper.setHeight(Ti.UI.SIZE);
        } else {
            Ti.API.error("hide menu");
            $.superWrapper.setHeight("47dp");
        }
    };
    $.superWrapper.addEventListener("click", function() {
        $.superWrapper.setHeight("47dp");
    });
    $.title && $.Wrapper.add($.title);
    __defers["$.__views.__alloyId1!click!config"] && $.__views.__alloyId1.addEventListener("click", config);
    __defers["$.__views.__alloyId2!click!help"] && $.__views.__alloyId2.addEventListener("click", help);
    __defers["$.__views.__alloyId3!click!terms"] && $.__views.__alloyId3.addEventListener("click", terms);
    __defers["$.__views.__alloyId4!click!close"] && $.__views.__alloyId4.addEventListener("click", close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;