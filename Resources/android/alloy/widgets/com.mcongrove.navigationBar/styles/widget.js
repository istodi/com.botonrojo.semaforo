function WPATH(s) {
    var index = s.lastIndexOf("/");
    var path = -1 === index ? "com.mcongrove.navigationBar/" + s : s.substring(0, index) + "/com.mcongrove.navigationBar/" + s.substring(index + 1);
    return true && 0 !== path.indexOf("/") ? "/" + path : path;
}

module.exports = [ {
    isApi: true,
    priority: 1000.0001,
    key: "TextField",
    style: {
        color: "#000",
        backgroundColor: "#FFF",
        borderWidth: 1,
        borderColor: "#bbb"
    }
}, {
    isId: true,
    priority: 100000.0002,
    key: "superWrapper",
    style: {
        top: "0dp",
        right: "0dp",
        height: "47dp",
        width: Ti.UI.FILL
    }
}, {
    isId: true,
    priority: 100000.0003,
    key: "Wrapper",
    style: {
        top: "0dp",
        left: "0dp",
        width: Ti.UI.FILL,
        height: "47dp",
        backgroundColor: "#e6e6e6"
    }
}, {
    isId: true,
    priority: 100000.0004,
    key: "border",
    style: {
        bottom: "0dp",
        height: "1dp",
        width: Ti.UI.FILL,
        backgroundColor: "#999999",
        opacity: .2
    }
}, {
    isId: true,
    priority: 100000.0005,
    key: "overlay",
    style: {
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        left: "0dp",
        top: "0dp"
    }
}, {
    isId: true,
    priority: 100000.0006,
    key: "left",
    style: {
        top: "0dp",
        left: "0dp",
        width: "48dp",
        height: "47dp"
    }
}, {
    isId: true,
    priority: 100000.0007,
    key: "leftImage",
    style: {
        top: "13dp",
        left: "9dp",
        width: "32dp",
        height: "21dp"
    }
}, {
    isId: true,
    priority: 100000.0008,
    key: "back",
    style: {
        top: "0dp",
        left: "0dp",
        width: "48dp",
        height: "47dp"
    }
}, {
    isId: true,
    priority: 100000.0009,
    key: "backImage",
    style: {
        top: "13dp",
        left: "9dp",
        width: "32dp",
        height: "21dp",
        preventDefaultImage: true
    }
}, {
    isId: true,
    priority: 100000.001,
    key: "next",
    style: {
        top: "0dp",
        right: "0dp",
        width: "48dp",
        height: "47dp"
    }
}, {
    isId: true,
    priority: 100000.0011,
    key: "nextImage",
    style: {
        top: "9dp",
        right: "9dp",
        width: "28dp",
        height: "28dp",
        preventDefaultImage: true
    }
}, {
    isId: true,
    priority: 100000.0012,
    key: "right",
    style: {
        top: "0dp",
        right: "0dp",
        width: "48dp",
        height: "47dp",
        zIndex: 1
    }
}, {
    isId: true,
    priority: 100000.0013,
    key: "rightImage",
    style: {
        top: "13dp",
        right: "9dp",
        width: "32dp",
        height: "21dp",
        borderRadius: 4
    }
}, {
    isId: true,
    priority: 100000.0014,
    key: "Nodes",
    style: {
        width: "150dp",
        height: Ti.UI.SIZE,
        top: "47dp",
        right: 0,
        backgroundColor: "#000",
        separatorColor: "#FFF"
    }
} ];