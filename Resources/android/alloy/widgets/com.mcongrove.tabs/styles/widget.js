function WPATH(s) {
    var index = s.lastIndexOf("/");
    var path = -1 === index ? "com.mcongrove.tabs/" + s : s.substring(0, index) + "/com.mcongrove.tabs/" + s.substring(index + 1);
    return true && 0 !== path.indexOf("/") ? "/" + path : path;
}

module.exports = [ {
    isApi: true,
    priority: 1000.0001,
    key: "TextField",
    style: {
        color: "#000",
        backgroundColor: "#FFF",
        borderWidth: 1,
        borderColor: "#bbb"
    }
}, {
    isId: true,
    priority: 100000.0022,
    key: "Wrapper",
    style: {
        height: "60dp",
        bottom: "0dp"
    }
}, {
    isId: true,
    priority: 100000.0023,
    key: "TabGroup",
    style: {
        height: "60dp",
        width: Ti.UI.FILL,
        bottom: "0dp"
    }
}, {
    isId: true,
    priority: 100000.0024,
    key: "Border",
    style: {
        top: "0dp",
        height: "1dp",
        width: Ti.UI.FILL,
        backgroundColor: "#000",
        opacity: .2
    }
}, {
    isId: true,
    priority: 100000.0025,
    key: "IndicatorContainer",
    style: {
        height: "59dp"
    }
}, {
    isId: true,
    priority: 100000.0026,
    key: "Indicator",
    style: {
        height: "59dp",
        bottom: "0dp",
        left: "0dp",
        backgroundColor: "#FFF",
        opacity: .4
    }
}, {
    isId: true,
    priority: 100000.0027,
    key: "Overlay",
    style: {
        width: Ti.UI.FILL,
        height: "60dp"
    }
}, {
    isId: true,
    priority: 100000.0028,
    key: "TabContainer",
    style: {
        height: "60dp"
    }
}, {
    isId: true,
    priority: 100000.0029,
    key: "Tabs",
    style: {
        layout: "horizontal",
        height: "60dp",
        width: Ti.UI.FILL
    }
}, {
    isId: true,
    priority: 100000.003,
    key: "TabGroupMore",
    style: {
        height: Ti.UI.SIZE,
        bottom: "60dp"
    }
}, {
    isId: true,
    priority: 100000.0031,
    key: "IndicatorMore",
    style: {
        height: "60dp",
        top: "0dp",
        left: "0dp",
        width: Ti.UI.FILL,
        backgroundColor: "#FFF",
        opacity: .4
    }
}, {
    isId: true,
    priority: 100000.0032,
    key: "TabContainerMore",
    style: {
        height: Ti.UI.SIZE,
        right: "0dp"
    }
}, {
    isId: true,
    priority: 100000.0033,
    key: "TabsMore",
    style: {
        layout: "vertical",
        height: Ti.UI.SIZE,
        bottom: "0dp"
    }
} ];